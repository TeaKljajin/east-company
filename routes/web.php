<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\BrandController;
use App\Http\Controllers\Admin\SliderController;
use App\Http\Controllers\Admin\AboutController;
use App\Http\Controllers\Admin\PortfolioController;
use App\Http\Controllers\Admin\ServiceController;
use App\Http\Controllers\Admin\TeamController;
use App\Http\Controllers\Admin\TestimonialController;
use App\Http\Controllers\Admin\ContactController;
use App\Http\Controllers\Admin\MessageController;
use App\Http\Controllers\Admin\SocialMediaController;
use App\Http\Controllers\Admin\NewsletterController;
use App\Http\Controllers\Admin\UserController;


use App\Http\Controllers\Front\FrontController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Frontend
Route::get('/', [FrontController::class,'index'])->name('index');



//Pages
Route::get('/portfolio/index', [FrontController::class,'portfolio'])->name('portfolio');
Route::get('/about', [FrontController::class,'about'])->name('about');
Route::get('/service', [FrontController::class,'service'])->name('service');
Route::get('/contact', [ FrontController::class,'contact'])->name('contact');



//Backend
Route::get('/dashboard', [DashboardController::class,'index'])->name('dashboard');
Route::get('/dashboard/logout', [DashboardController::class,'logout'])->name('logout');

//Clients
Route::get('/brand/index', [BrandController::class,'index'])->name('brand.index');
Route::get('/brand/edit/{id}', [BrandController::class,'edit'])->name('brand.edit');
Route::post('/brand/store', [BrandController::class,'store'])->name('brand.store');
Route::post('/brand/update/{id}', [BrandController::class,'update'])->name('brand.update');
Route::get('/brand/delete/{id}', [BrandController::class,'delete'])->name('brand.delete');
Route::get('/brand/restore/{id}', [BrandController::class,'restore'])->name('brand.restore');
Route::get('/brand/permanentlyDeleted/{id}', [BrandController::class,'permanentlyDeleted'])->name('brand.permanentlyDeleted');
Route::get('/brand/trashed', [BrandController::class,'trashed'])->name('brand.trashed');

//Slider
Route::get('/slider/index', [SliderController::class,'index'])->name('slider.index');
Route::post('/slider/store', [SliderController::class,'store'])->name('slider.store');
Route::get('/slider/edit/{id}', [SliderController::class,'edit'])->name('slider.edit');
Route::post('/slider/update/{id}', [SliderController::class,'update'])->name('slider.update');
Route::get('/slider/delete/{id}', [SliderController::class,'delete'])->name('slider.delete');

//About
Route::get('/about/index', [AboutController::class,'index'])->name('about.index');
Route::get('/about/edit/{id}', [AboutController::class,'edit'])->name('about.edit');
Route::post('/about/update/{id}', [AboutController::class,'update'])->name('about.update');
Route::get('/about/delete/{id}', [AboutController::class,'delete'])->name('about.delete');
Route::post('/about/store', [AboutController::class,'store'])->name('about.store');

//Portfolio
Route::get('/portfolio', [PortfolioController::class,'index'])->name('portfolio.index');
Route::post('/portfolio/store', [PortfolioController::class,'store'])->name('portfolio.store');
Route::post('/portfolio/delete/{id}', [PortfolioController::class,'delete'])->name('portfolio.delete');


//Service
Route::get('/service/index', [ServiceController::class,'index'])->name('service.index');
Route::get('/service/edit/{id}', [ServiceController::class,'edit'])->name('service.edit');
Route::post('/service/update/{id}', [ServiceController::class,'update'])->name('service.update');
Route::post('/service/store', [ServiceController::class,'store'])->name('service.store');
Route::get('/service/delete/{id}', [ServiceController::class,'delete'])->name('service.delete');


//Team
Route::get('/team/index', [TeamController::class,'index'])->name('team.index');
Route::get('/team/edit/{id}', [TeamController::class,'edit'])->name('team.edit');
Route::post('/team/store', [TeamController::class,'store'])->name('team.store');
Route::post('/team/update/{id}', [TeamController::class,'update'])->name('team.update');
Route::get('/team/delete/{id}', [TeamController::class,'delete'])->name('team.delete');

//Testimonial
Route::get('/testimonial/index', [TestimonialController::class,'index'])->name('testimonial.index');
Route::get('/testimonial/edit/{id}', [TestimonialController::class,'edit'])->name('testimonial.edit');
Route::post('/testimonial/store', [TestimonialController::class,'store'])->name('testimonial.store');
Route::post('/testimonial/update/{id}', [TestimonialController::class,'update'])->name('testimonial.update');
Route::get('/testimonial/delete/{id]', [TestimonialController::class,'delete'])->name('testimonial.delete');


//Contact//Profile
Route::get('/contact/index', [ContactController::class,'index'])->name('contact.index');
Route::post('/contact/store', [ContactController::class,'store'])->name('contact.store');
Route::get('/contact/edit/{id}', [ContactController::class,'edit'])->name('contact.edit');
Route::post('/contact/update/{id}', [ContactController::class,'update'])->name('contact.update');
Route::get('/contact/deleteContact/{id}', [ContactController::class,'delete'])->name('contact.delete');

//Contact//Messages
Route::get('/contact/messages', [ MessageController::class,'index'])->name('message.index');
Route::post('/contact/messages/store', [ MessageController::class,'store'])->name('message.store');
Route::get('/contact/approve/{id}', [ MessageController::class,'approve'])->name('message.approve');
Route::get('/contact/disapprove/{id}', [ MessageController::class,'disapprove'])->name('message.disapprove');
Route::get('/contact/delete/{id}', [ MessageController::class,'delete'])->name('message.delete');

//Social Media
Route::get('/social-media', [ SocialMediaController::class,'index'])->name('social.media.index');
Route::post('/social-media/facebook', [ SocialMediaController::class,'facebook'])->name('facebook');
Route::get('/social-media/facebook/delete/{id}', [ SocialMediaController::class,'facebook_delete'])->name('facebook.delete');
Route::post('/social-media/twitter', [ SocialMediaController::class,'twitter'])->name('twitter');
Route::get('/social-media/twitter/delete/{id}', [ SocialMediaController::class,'twitter_delete'])->name('twitter.delete');
Route::post('/social-media/instagram', [ SocialMediaController::class,'instagram'])->name('instagram');
Route::get('/social-media/instagram/delete/{id}', [ SocialMediaController::class,'instagram_delete'])->name('instagram.delete');
Route::post('/social-media/pinterest', [ SocialMediaController::class,'pinterest'])->name('pinterest');
Route::get('/social-media/pinterest/delete/{id}', [ SocialMediaController::class,'pinterest_delete'])->name('pinterest.delete');


//Newsletters
Route::get('/newsletter', [ NewsletterController::class,'index'])->name('newsletter.index');
Route::post('/newsletter/store', [ NewsletterController::class,'store'])->name('newsletter.store');
Route::get('/newsletter/delete/{id}', [ NewsletterController::class,'delete'])->name('newsletter.delete');


//Profile
Route::get('/profile', [ UserController::class,'index'])->name('profile.index');
Route::get('/profile/change-password', [ UserController::class,'change_password'])->name('profile.change.password');
Route::post('/profile/changePassword/{id}', [ UserController::class,'changePassword'])->name('profile.changePassword');
Route::post('/profile/update/{id}', [ UserController::class,'update'])->name('profile.update');
