@extends('admin.layout.master')
@section('content')



    <h2 style="text-align: center;padding-bottom: 2%;">Edit About</h2>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg" style="padding:10px;">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">Edit About</div>
                            <div class="card-body">
                                <form method="POST" action="{{route('about.update', $about->id)}}">
                                    @csrf
                                    <div class="mb-3">
                                        <label for="title" class="form-label">Title</label>
                                        <input type="text" class="form-control" id="title" name="title" aria-describedby="title"
                                               style="opacity: 0.7" value="{{$about->title}}">
                                        @error('title')
                                        <div style="color:red;">{{ $message }}</div>
                                        @enderror
                                        @if(session('errorMessage'))
                                            <div style="color:red;">{{ session('errorMessage') }}</div>
                                        @endif
                                    </div>
                                    <div class="mb-3">
                                        <label for="short_des" class="form-label">Short Description</label>
                                        <textarea type="text" rows="3" cols="10" class="form-control" id="short_des" name="short_des" aria-describedby="short_des">value="{{$about->short_des}}"</textarea>
                                        @error('short_des')
                                        <div style="color:red;">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <label for="long_des" class="form-label">Long Description</label>
                                        <textarea type="text" rows="10" cols="20" class="form-control" id="long_des" name="long_des" aria-describedby="long_des">value="{{$about->long_des}}"</textarea>

                                        @error('long_des')
                                        <div style="color:red;">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <button type="submit" class="btn btn-success">Submit</button>
                                    <a href="{{route('about.index')}}" class="btn btn-outline-primary btn-md " style="float:right" role="button" aria-pressed="true">Go Back</a>

                                </form>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

@endsection

