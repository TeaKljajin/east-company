@extends('admin.layout.master')
@section('content')



    <h2>About</h2>
    <div class="py-12 my-5">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg" style="padding:10px;">

                @if(count($abouts) >0)
                <div class="row">
                    <div class="col-md-12" style="padding:2%;">
                        @foreach($abouts as $about)
                            <a href="{{route('about.edit',$about->id)}}" class="btn btn-primary btn-md "   role="button" aria-pressed="true">Edit</a>
                            <a href="{{route('about.delete',$about->id)}}" class="btn btn-danger btn-md "   role="button" aria-pressed="true">Delete</a>
                            <div style="text-align: center;width:90%;">
                                <h1>{{$about->title}}</h1><br><hr>
                            <div class="row">
                                <div class="col-lg-6">
                                    <b>Short description:</b>
                                    <div>{{$about->short_des}}</div>
                                </div>
                                <br>
                                <div class="col-lg-6">
                                    <b>Long description: </b>
                                    <div >{{$about->long_des}}</div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                @else
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">About</div>
                                <div class="card-body">
                                    <form method="POST" action="{{route('about.store')}}" >
                                        @csrf
                                        <div class="mb-3">
                                            <label for="title" class="form-label">Title</label>
                                            <input type="text" class="form-control" id="title" name="title" aria-describedby="title"
                                                   style="opacity: 0.7">
                                            @error('title')
                                            <div style="color:red;">{{ $message }}</div>
                                            @enderror
                                            @if(session('errorMessage'))
                                                <div style="color:red;">{{ session('errorMessage') }}</div>
                                            @endif
                                        </div>
                                        <div class="mb-3">
                                            <label for="short_des" class="form-label">Short Description</label>
                                            <textarea type="text" rows="3" cols="10" class="form-control" id="short_des" name="short_des" aria-describedby="short_des"></textarea>
                                            @error('short_des')
                                            <div style="color:red;">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="long_des" class="form-label">Long Description</label>
                                            <textarea type="text" rows="10" cols="20" class="form-control" id="long_des" name="long_des" aria-describedby="long_des"></textarea>

                                            @error('long_des')
                                            <div style="color:red;">{{ $message }}</div>
                                            @enderror
                                        </div>

                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

@endsection

