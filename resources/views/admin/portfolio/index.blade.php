@extends('admin.layout.master')
@section('content')

    <h2 style="text-align: center;padding-bottom: 2%;">All Portfolios</h2>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg" style="padding:10px;">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">Add Portfolio</div>
                            <div class="card-body">
                                <form method="POST" action="{{route('portfolio.store')}}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="mb-3">
                                        <label for="images" class="form-label">Image</label>
                                        <input type="file" class="form-control" id="images" name="images[]" multiple="" aria-describedby="images">
                                        @error('image')
                                        <div style="color:red;">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">All Portfolios</div>
                            <div class="card-body">

                                @if(count($portfolios) >= 1)
                                    <div class="card-group">
                                        @foreach($portfolios as $portfolio)
                                            <div class="col-md-4">
                                                <img src="{{ asset($portfolio->images) }}" style="height: 200px;width:200px;">
                                                <div style="padding-bottom:10px;"><form action="{{route('portfolio.delete',$portfolio->id)}}" method="POST">
                                                        @csrf
                                                        <input type="submit" style="padding:5px;width:200px;" value="Delete Image">
                                                    </form>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div style="background: lightgrey;padding:5px;color:white;border-radius: 5px;">{{$portfolios->render()}}</div>
                                @else
                                    <h6>Uploaded Images will apear hear. Currently there is no portfolio images. </h6>
                                @endif
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

