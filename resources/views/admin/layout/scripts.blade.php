
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDCn8TFXGg17HAUcNpkwtxxyT9Io9B_NcM" defer></script>
<script src="{{asset('backendDashboard/assets/plugins/jquery/jquery.min.js')}}"></script>
<script src="{{asset('backendDashboard/assets/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('backendDashboard/assets/plugins/toaster/toastr.min.js')}}"></script>
<script src="{{asset('backendDashboard/assets/plugins/slimscrollbar/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('backendDashboard/assets/plugins/charts/Chart.min.js')}}"></script>
<script src="{{asset('backendDashboard/assets/plugins/ladda/spin.min.js')}}"></script>
<script src="{{asset('backendDashboard/assets/plugins/ladda/ladda.min.js')}}"></script>
<script src="{{asset('backendDashboard/assets/plugins/jquery-mask-input/jquery.mask.min.js')}}"></script>
<script src="{{asset('backendDashboard/assets/plugins/select2/js/select2.min.js')}}"></script>
<script src="{{asset('backendDashboard/assets/plugins/jvectormap/jquery-jvectormap-2.0.3.min.js')}}"></script>
<script src="{{asset('backendDashboard/assets/plugins/jvectormap/jquery-jvectormap-world-mill.js')}}"></script>
<script src="{{asset('backendDashboard/assets/plugins/daterangepicker/moment.min.js')}}"></script>
<script src="{{asset('backendDashboard/assets/plugins/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('backendDashboard/assets/plugins/jekyll-search.min.js')}}"></script>
<script src="{{asset('backendDashboard/assets/js/sleek.js')}}"></script>
<script src="{{asset('backendDashboard/assets/js/chart.js')}}"></script>
<script src="{{asset('backendDashboard/assets/js/date-range.js')}}"></script>
<script src="{{asset('backendDashboard/assets/js/map.js')}}"></script>
<script src="{{asset('backendDashboard/assets/js/custom.js')}}"></script>

<!---------Toaster Message-------------->
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script>
<script>
    @if(session()->has('message'))
        var type = "{{session()->get('alert-type','info')}}"
        switch(type){
            case 'info':
                toastr.info("{{session()->get('message')}}");
                break;
            case 'success':
                toastr.success("{{session()->get('message')}}");
                break;
            case 'warning':
                toastr.warning("{{session()->get('message')}}");
                break;

        }
    @endif

</script>



</body>
</html>

