<aside class="left-sidebar bg-sidebar">
    <div id="sidebar" class="sidebar sidebar-with-footer">
        <!-- Aplication Brand -->
        <div class="app-brand">
            <a href="#">
                <svg
                    class="brand-icon"
                    xmlns="http://www.w3.org/2000/svg"
                    preserveAspectRatio="xMidYMid"
                    width="30"
                    height="33"
                    viewBox="0 0 30 33"
                >
                    <g fill="none" fill-rule="evenodd">
                        <path
                            class="logo-fill-blue"
                            fill="#7DBCFF"
                            d="M0 4v25l8 4V0zM22 4v25l8 4V0z"
                        />
                        <path class="logo-fill-white" fill="#FFF" d="M11 4v25l8 4V0z" />
                    </g>
                </svg>
                <span class="brand-name">Admin Dashboard</span>
            </a>
        </div>
        <!-- begin sidebar scrollbar -->
        <div class="sidebar-scrollbar">

            <!-- sidebar menu -->
            <ul class="nav sidebar-inner" id="sidebar-menu">
                <li>
                    <a class="sidenav-item-link" href="{{route('dashboard')}}">
                        <i class="mdi mdi-view-dashboard-outline"></i>
                        <span class="nav-text">Dashboard</span> <b class="caret"></b>
                    </a>
                </li>

                <li  class="has-sub" >
                    <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#pages"
                       aria-expanded="false" aria-controls="pages">
                        <i class="mdi mdi-folder-multiple-outline"></i>
                        <span class="nav-text">Clients</span> <b class="caret"></b>
                    </a>
                    <ul  class="collapse"  id="pages"
                         data-parent="#sidebar-menu">
                        <div class="sub-menu">

                            <li >
                                <a class="sidenav-item-link" href="{{route('brand.index')}}">
                                    <span class="nav-text">All Clients</span>

                                </a>
                            </li>
                            <li >
                                <a class="sidenav-item-link" href="{{route('brand.trashed')}}">
                                    <span class="nav-text">Trashed Clients</span>

                                </a>
                            </li>

                        </div>
                    </ul>
                </li>


                <li>
                    <a class="sidenav-item-link" href="{{route('slider.index')}}">
                        <i class="mdi mdi-view-dashboard"></i>
                        <span class="nav-text">Slider</span> <b class="caret"></b>
                    </a>
                </li>

                <li>
                    <a class="sidenav-item-link" href="{{route('about.index')}}">
                        <i class="mdi mdi-book-open-page-variant"></i>
                        <span class="nav-text">About</span> <b class="caret"></b>
                    </a>
                </li>
                <li>
                    <a class="sidenav-item-link" href="{{route('portfolio.index')}}">
                        <i class="mdi mdi-image-filter-none"></i>
                        <span class="nav-text">Portfolio</span> <b class="caret"></b>
                    </a>
                </li>
                <li>
                    <a class="sidenav-item-link" href="{{route('service.index')}}">
                        <i class="mdi mdi-image-filter-none"></i>
                        <span class="nav-text">Service</span> <b class="caret"></b>
                    </a>
                </li>
                <li>
                    <a class="sidenav-item-link" href="{{route('team.index')}}">
                        <i class="mdi mdi-image-filter-none"></i>
                        <span class="nav-text">Team</span> <b class="caret"></b>
                    </a>
                </li>
                <li>
                    <a class="sidenav-item-link" href="{{route('testimonial.index')}}">
                        <i class="mdi mdi-chart-pie"></i>
                        <span class="nav-text">Testimonial</span> <b class="caret"></b>
                    </a>
                </li>
                <li  class="has-sub" >
                    <a class="sidenav-item-link" href="#" data-toggle="collapse" data-target="#charts"
                       aria-expanded="false" aria-controls="charts">
                        <i class="mdi mdi-book-open-page-variant"></i>
                        <span class="nav-text">Contact</span> <b class="caret"></b>
                    </a>
                    <ul  class="collapse"  id="charts"
                         data-parent="#sidebar-menu">
                        <div class="sub-menu">
                            <li>
                                <a class="sidenav-item-link" href="{{route('contact.index')}}">
                                    <span class="nav-text">Company Profile</span>
                                </a>
                            </li>
                            <li >
                                <a class="sidenav-item-link" href="{{route('message.index')}}">
                                    <span class="nav-text">Messages</span>
                                </a>
                            </li>
                        </div>
                    </ul>
                </li>

                <li>
                    <a class="sidenav-item-link" href="{{route('social.media.index')}}">
                        <i class="mdi mdi-chart-pie"></i>
                        <span class="nav-text">Social Media </span> <b class="caret"></b>
                    </a>
                </li>
                <li>
                    <a class="sidenav-item-link" href="{{route('newsletter.index')}}">
                        <i class="mdi mdi-chart-pie"></i>
                        <span class="nav-text">Newsletters </span> <b class="caret"></b>
                    </a>
                </li>

            </ul>
        </div>

        <hr class="separator" />


    </div>
</aside>

