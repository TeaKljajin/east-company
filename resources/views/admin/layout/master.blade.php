<!------HEAD--------->
@include('admin.layout.head')
<body class="sidebar-fixed sidebar-dark header-light header-fixed" id="body">
<script>
    NProgress.configure({ showSpinner: false });
    NProgress.start();
</script>
<div class="mobile-sticky-body-overlay"></div>
<div class="wrapper">
    <!--------SIDEBAR------->
    @include('admin.layout.sidebar')

    <div class="page-wrapper">
        <!-- Header -->
        <header class="main-header " id="header">
            <!--------NAVBAR-------->
            @include('admin.layout.navbar')
        </header>


        <!-----------MAIN CONTENT--------->
        <div class="content-wrapper">
            <div class="content">
                @yield('content')
            </div>
        </div>
        <!-----------END MAIN CONTENT--------->

        <!------FOOTER--------->
        @include('admin.layout.footer')

    </div>
</div>

<!-------SCRIPTS--------->
@include('admin.layout.scripts')



