@extends('admin.layout.master')
@section('content')

    <h2 style="text-align: center;padding-bottom: 2%;">All Team</h2>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg" style="padding:10px;">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">Add Team Member</div>
                            <div class="card-body">
                                <form method="POST" action="{{route('team.store')}}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="mb-3">
                                        <label for="name" class="form-label">Name</label>
                                        <input type="text" class="form-control" id="name" name="name" aria-describedby="name">
                                        @error('name')
                                        <div style="color:red;">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <label for="position" class="form-label">Position in Company</label>
                                        <input type="text" class="form-control" id="position" name="position" aria-describedby="position">
                                        @error('position')
                                        <div style="color:red;">{{ $message }}</div>
                                        @enderror

                                    </div>
                                    <div class="mb-3">
                                        <label for="image" class="form-label">Team Member Image</label>
                                        <input type="file" class="form-control" id="image" name="image" aria-describedby="image">
                                        @error('image')
                                        <div style="color:red;">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <button type="submit" class="btn btn-success">Submit</button>

                                </form>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">All Clients</div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Serial No</th>
                                        <th scope="col" style="width:20%">Name</th>
                                        <th scope="col" style="width:25%">Position In Compsny</th>
                                        <th>Image</th>
                                        <th scope="col">Created_at</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($teams)
                                        @foreach($teams as $team)
                                            <tr>
                                                <th scope="row">{{$teams->firstItem()+$loop->index}}</th>
                                                <td>{{ $team->name}}</td>
                                                <td>{{ $team->position}}</td>
                                                <td><img src="{{ asset($team->image) }}" style="height: 60px;width:70px;"></td>
                                                <td>{{ $team->created_at->diffForHumans()}}</td>
                                                <td>
                                                    <a href="{{route('team.edit',$team->id)}}" class="btn btn-small btn-info">Edit</a>
                                                    <a href="{{route('team.delete',$team->id)}}" onclick="return confirm('Are you sure to delete?')" class="btn btn-small btn-danger">Delete</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <h4>Currently there are no team available</h4>
                                    @endif

                                    </tbody>
                                </table>
                                {{$teams->render()}}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

