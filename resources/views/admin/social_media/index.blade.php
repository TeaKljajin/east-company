@extends('admin.layout.master')
@section('content')



    <h2 style="text-align: center;padding-bottom: 2%;">Social Media Links</h2>

    <div class="row" style="width: 60%;">
        <div class="col-lg-2">
            @if($social->facebook == 'null')
                <h4>Facebook</h4><br>
            @else

            @endif
            @if($social->twitter == 'null')
                    <h4>Twitter</h4><br>
                @else
                @endif

                @if($social->instagram == 'null')
                    <h4>Instagram</h4><br>
                @else
                @endif

                @if($social->pinterest == 'null')
                    <h4>Pinterest</h4>
                @else
                @endif
        </div>
        <div class="col-lg-10">
            @if($social->facebook == 'null')
                <form class="form-inline" action="{{route('facebook')}}" method="POST">
                    @csrf
                    <div class="form-group mx-sm-3 mb-2" style="width: 70%;">
                        <input type="url" name="facebook" style="width: 100%;height:40px;border:none;border-radius: 10px;padding-left: 15px" >
                    </div>
                    <button type="submit" class="btn btn-primary mb-2">Save</button>
                </form>
            @else
            @endif
            @if($social->twitter == 'null')
            <form class="form-inline" action="{{route('twitter')}}" method="POST">
                @csrf
                <div class="form-group mx-sm-3 mb-2" style="width: 70%;">
                    <input type="url" name="twitter" style="width: 100%;height:40px;border:none;border-radius: 10px;padding-left: 15px" >
                </div>
                <button type="submit" class="btn btn-primary mb-2">Save</button>
            </form>
            @endif


                @if($social->instagram == 'null')
                <form class="form-inline" action="{{route('instagram')}}" method="POST">
                @csrf
                <div class="form-group mx-sm-3 mb-2" style="width: 70%;">
                    <input type="url" name="instagram" style="width: 100%;height:40px;border:none;border-radius: 10px;padding-left: 15px" >
                </div>
                <button type="submit" class="btn btn-primary mb-2">Save</button>
                </form>
                 @endif

                @if($social->pinterest == 'null')
                <form class="form-inline" action="{{route('pinterest')}}" method="POST">
                @csrf
                <div class="form-group mx-sm-3 mb-2" style="width: 70%;">
                    <input type="url" name="pinterest" style="width: 100%;height:40px;border:none;border-radius: 10px;padding-left: 15px" >
                </div>
                <button type="submit" class="btn btn-primary mb-2">Save</button>
               </form>
                @endif
        </div>
    </div>
<hr>
  @if($social->facebook == 'null')
  @else
      <div class="row mt-5" style="width: 70%;">
          <div class="col-lg-10">
              <h4 >Facebook : {{$social->facebook}}</h4>
          </div>
          <div class="col-lg-2">
              <a href="{{route('facebook.delete', $social->id)}}" class="btn btn-danger btn-md "   role="button" aria-pressed="true">Delete</a>
          </div>
      </div>
      <hr style="width: 55%;">
  @endif
    @if($social->twitter == 'null')
    @else
        <div class="row" style="width: 70%;">
            <div class="col-lg-10">
                <h4>Twitter : {{$social->twitter}}</h4>
            </div>
            <div class="col-lg-2">
                <a href="{{route('twitter.delete', $social->id)}}" class="btn btn-danger btn-md "   role="button" aria-pressed="true">Delete</a>
            </div>
        </div>
        <hr style="width: 55%;">
    @endif

    @if($social->instagram == 'null')
    @else
        <div class="row" style="width: 70%;">
            <div class="col-lg-10">
                <h4>Instagram : {{Str::limit($social->instagram, 70)}}</h4>
            </div>
            <div class="col-lg-2">
                <a href="{{route('instagram.delete',$social->id)}}" class="btn btn-danger btn-md "   role="button" aria-pressed="true">Delete</a>
            </div>
        </div>
        <hr style="width: 55%;">
    @endif
    @if($social->pinterest == 'null')
    @else
        <div class="row" style="width: 70%;">
            <div class="col-lg-10">
                <h4>Pinterest : {{$social->pinterest}}</h4>
            </div>
            <div class="col-lg-2">
                <a href="{{route('pinterest.delete', $social->id)}}" class="btn btn-danger btn-md "   role="button" aria-pressed="true">Delete</a>
            </div>
        </div>
    @endif

@endsection



