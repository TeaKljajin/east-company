@extends('admin.layout.master')
@section('content')

    <h2 style="text-align: center;padding-bottom: 2%;">Slider</h2>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg" style="padding:10px;">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">Add New Slider</div>
                            <div class="card-body">
                                <form method="POST" action="{{route('slider.store')}}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="mb-3">
                                        <label for="title" class="form-label">Title</label>
                                        <input type="text" class="form-control" id="title" name="title" aria-describedby="brand_name"
                                               value="Add New Title" style="opacity: 0.7" onfocus="this.value=''">
                                        @error('title')
                                        <div style="color:red;">{{ $message }}</div>
                                        @enderror
                                        @if(session('errorMessage'))
                                            <div style="color:red;">{{ session('errorMessage') }}</div>
                                        @endif
                                    </div>
                                    <div class="mb-3">
                                        <label for="description" class="form-label">Description</label>
                                        <textarea type="text" class="form-control" rows="5" cols="5" id="description" name="description" aria-describedby="description"></textarea>
                                        @error('description')
                                        <div style="color:red;">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <label for="image" class="form-label">Image</label>
                                        <input type="file" class="form-control" id="image" name="image" aria-describedby="image">
                                        @error('image')
                                        <div style="color:red;">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">Sliders</div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Serial No</th>
                                        <th scope="col" style="width:15%">Title</th>
                                        <th scope="col" style="width:30%">Description</th>
                                        <th scope="col">Image</th>
                                        <th scope="col">Created_at</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($sliders)
                                        @foreach($sliders as $slider)
                                            <tr>
                                                <th scope="row">{{$sliders->firstItem()+$loop->index}}</th>
                                                <td>{{ $slider->title}}</td>
                                                <td style="width:30%;">{{ Str::limit($slider->description, 170)}}</td>
                                                <td><img src="{{ asset($slider->image) }}" style="height: 60px;width:70px;" alt="Image"></td>
                                                <td>{{ $slider->created_at->diffForHumans()}}</td>
                                                <td>
                                                    <a href="{{route('slider.edit',$slider->id)}}" class="btn btn-small btn-info">Edit</a>
                                                    <a href="{{route('slider.delete',$slider->id)}}" onclick="return confirm('Are you sure to delete?')" class="btn btn-small btn-danger">Delete</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <h4>Currently there are no sliders available</h4>
                                    @endif

                                    </tbody>
                                </table>
                                {{$sliders->render()}}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
