@extends('admin.layout.master')
@section('content')

    <h2 style="text-align: center;padding-bottom: 2%;">Edit Slider</h2>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg" style="padding:10px;">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">Edit Slider</div>
                            <div class="card-body">
                                <form method="POST" action="{{route('slider.update', $slider->id)}}" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="old_image" value="{{$slider->image}}">
                                    <div class="mb-3">
                                        <label for="title" class="form-label">Title</label>
                                        <input type="text" class="form-control" id="title" name="title" aria-describedby="title"
                                               value="{{$slider->title}}" style="opacity: 0.7" >
                                        @error('title')
                                        <div style="color:red;">{{ $message }}</div>
                                        @enderror
                                        @if(session('errorMessage'))
                                            <div style="color:red;">{{ session('errorMessage') }}</div>
                                        @endif
                                    </div>
                                    <div class="mb-3">
                                        <label for="description" class="form-label">Description</label>
                                        <textarea type="text" class="form-control" id="description" rows="5" cols="5" name="description"
                                               style="opacity: 0.7">{{$slider->description}}</textarea>
                                    </div>
                                    <div class="mb-3">
                                        <label for="image" class="form-label">Image</label>
                                        <input type="file" class="form-control" id="image" name="image" aria-describedby="image">
                                        @error('image')
                                        <div style="color:red;">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group mb-2">
                                        <img src="{{asset($slider->image)}}" style="height:250px;width:100%;">
                                    </div>
                                    <button type="submit" class="btn btn-success">Submit</button>
                                    <a href="{{route('slider.index')}}" class="btn btn-outline-primary btn-md " style="float:right" role="button" aria-pressed="true">Create New Slider</a>

                                </form>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">Sliders</div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Serial No</th>
                                        <th scope="col">Title</th>
                                        <th scope="col">Description</th>
                                        <th scope="col">Image</th>
                                        <th scope="col">Created_at</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($sliders)
                                        @foreach($sliders as $slider)
                                            <tr>
                                                <th scope="row">{{$sliders->firstItem()+$loop->index}}</th>
                                                <td>{{ $slider->title}}</td>
                                                <td style="width:30%;">{{ Str::limit($slider->description, 270)}}</td>
                                                <td><img src="{{ asset($slider->image) }}" style="height: 60px;width:70px;" alt="Image"></td>
                                                <td>{{ $slider->created_at->diffForHumans()}}</td>
                                                <td>
                                                    <a href="{{route('slider.edit',$slider->id)}}" class="btn btn-small btn-info">Edit</a>
                                                    <a href="{{route('slider.delete',$slider->id)}}" onclick="return confirm('Are you sure to delete?')" class="btn btn-small btn-danger">Delete</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <h4>Currently there are no sliders available</h4>
                                    @endif

                                    </tbody>
                                </table>
                                {{$sliders->render()}}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

