@extends('admin.layout.master')
@section('content')


    <h2 style="text-align: center;padding-bottom: 2%;">All Subscribers</h2>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Subscribers</div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Serial No</th>
                            <th scope="col">Email</th>
                            <th scope="col">Created_at</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($newsletters)
                            @foreach($newsletters as $newsletter)
                                <tr>
                                    <th scope="row">{{$newsletters->firstItem()+$loop->index}}</th>
                                    <td>{{ $newsletter->email}}</td>
                                    <td>{{ $newsletter->created_at->diffForHumans()}}</td>
                                    <td>
                                        <a href="{{route('newsletter.delete',$newsletter->id)}}" onclick="return confirm('Are you sure to delete?')" class="btn btn-small btn-danger">Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <h4>Currently there are no newsletter available</h4>
                        @endif

                        </tbody>
                    </table>
                    {{$newsletters->render()}}
                </div>

            </div>
        </div>

        </div>






@endsection



