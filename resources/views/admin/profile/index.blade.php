@extends('admin.layout.master')
@section('content')

    <h2 style="text-align: center;padding-bottom: 2%;">Admin Profile</h2>

    <div class="content-wrapper">
        <div class="content">
            <div class="bg-white border rounded">
                <div class="row no-gutters">
                    <div class="col-lg-4 col-xl-3">
                        <div class="profile-content-left pt-5  px-3 px-xl-5">
                            <div class="card text-center widget-profile px-0 border-0">
                                <div class="card-img mx-auto rounded-circle">
                                    @if(Auth::user()->profile_photo_path)
                                        <img src="{{asset($user->profile_photo_path)}}" style="height:100px;width:100%;">
                                    @endif
                                </div>
                                <div class="card-body">
                                    <h4 class="py-2 text-dark">{{Auth::user()->name}}</h4>
                                    <p>{{Auth::user()->email}}</p>
                                </div>
                            </div>
                            <hr class="w-100">
                            <div class="contact-info pt-4">
                                <h5 class="text-dark mb-1">Contact Information</h5>
                                <p class="text-dark font-weight-medium pt-4 mb-2">Email address</p>
                                <p>{{Auth::user()->email}}</p>
                                <p class="text-dark font-weight-medium pt-4 mb-2">Phone Number</p>
                                <p>+99 9539 2641 31</p>
                                <p class="text-dark font-weight-medium pt-4 mb-2">Birthday</p>
                                <p>Nov 15, 1990</p>
                                <p class="text-dark font-weight-medium pt-4 mb-2">Social Profile</p>
                                <p class="pb-3 social-button">
                                    @if($social->twitter == 'null')
                                    @else
                                    <a href="{{$social->twitter}}" class="mb-1 btn btn-outline btn-twitter rounded-circle">
                                        <i class="mdi mdi-twitter"></i>
                                    </a>
                                    @endif
                                    @if($social->instagram == 'null')
                                        @else
                                            <a href="{{$social->instagram}}" class="mb-1 btn btn-outline btn-instagram rounded-circle">
                                                <i class="mdi mdi-instagram"></i>
                                            </a>
                                        @endif
                                        @if($social->facebook == 'null')
                                        @else
                                            <a href="{{$social->facebook}}" class="mb-1 btn btn-outline btn-facebook rounded-circle">
                                                <i class="mdi mdi-facebook"></i>
                                            </a>
                                        @endif
                                        @if($social->pinterest == 'null')
                                        @else
                                            <a href="{{$social->pinterest}}" class="mb-1 btn btn-outline btn-pinterest rounded-circle">
                                                <i class="mdi mdi-pinterest"></i>
                                            </a>
                                        @endif
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-xl-9">
                        <div class="profile-content-right py-5">
                            <ul class="nav nav-tabs px-3 px-xl-5 nav-style-border" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="timeline-tab" data-toggle="tab" href="#timeline" role="tab" aria-controls="timeline" aria-selected="true">Timeline</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Profile</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="settings-tab" data-toggle="tab" href="#settings" role="tab" aria-controls="settings" aria-selected="false">Settings</a>
                                </li>
                            </ul>
                            <div class="tab-content px-3 px-xl-5" id="myTabContent">
                                <div class="tab-pane fade show active" id="timeline" role="tabpanel" aria-labelledby="timeline-tab" style="padding: 15%">
                                  <h1>Here should be my notes</h1>
                                </div>
                                <div class="tab-pane fade mt-5" id="profile" role="tabpanel" aria-labelledby="profile-tab" style="padding: 2%">
                                    <h4>Edit Profile</h4>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <form method="POST" action="{{route('profile.update', $user->id)}}" enctype="multipart/form-data">
                                                @csrf
                                                <div class="mb-3">
                                                    <label for="name" class="form-label">Admin Name</label>
                                                    <input type="text" class="form-control" id="name" name="name" aria-describedby="name" value="{{$user->name}}">
                                                    @error('name')
                                                    <div style="color:red;">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="mb-3">
                                                    <label for="email" class="form-label">Admin Email</label>
                                                    <input type="email" class="form-control" id="email" name="email" aria-describedby="email" value="{{$user->email}}">
                                                    @error('email')
                                                    <div style="color:red;">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="mb-3">
                                                    <label for="image" class="form-label">Profile Image</label>
                                                    <input type="file" class="form-control" id="image" name="image" aria-describedby="image">
                                                    @error('image')
                                                    <div style="color:red;">{{ $message }}</div>
                                                    @enderror
                                                </div>

                                                <button type="submit" class="btn btn-success">Submit</button>
                                            </form>
                                        </div>
                                        <div class="col-lg-6">
                                            <img src="{{asset($user->profile_photo_path)}}" style="height:350px;width:70%;border-radius: 30px;">
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="settings" role="tabpanel" aria-labelledby="settings-tab">...</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div></div>
    </div>



@endsection
