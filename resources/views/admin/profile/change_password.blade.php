@extends('admin.layout.master')
@section('content')


    <h2 style="text-align: center;padding-bottom: 2%;">Change Password</h2>
    <div style="margin-left: 20%">
    <form method="POST" action="{{route('profile.changePassword', $user->id)}}" enctype="multipart/form-data" style="width: 70%;margin-top: 15px">
        @csrf
        <div class="mb-3">
            <label for="password" class="form-label">Current Password</label>
            <input type="password" id="current_password"  name="password" class="form-control" >
            @if(session()->has('old'))
                <div style="color:red;">{{session()->get('old')}}</div>
            @endif
            @error('password')

                    <div style="color:red;">{{ $message }}</div>

            @enderror
        </div>
        <div class="mb-3">
            <label for="new_password" class="form-label">New Password</label>
            <input type="password" id="password" name="new_password" class="form-control" >
            @if(session()->has('new'))
                <div style="color:red;">{{session()->get('new')}}</div>
            @endif
            @error('new_password')
                <div style="color:red;">{{ $message }}</div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="password_confirmation" class="form-label">Confirm New Password</label>
            <input type="password" id="password_confirmation" name="password_confirmation" class="form-control">
            @if(session()->has('conf'))
                <div style="color:red;">{{session()->get('conf')}}</div>
            @endif
            @error('password_confirmation')
            <div style="color:red;">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Save</button>

    </form>
    </div>


@endsection
