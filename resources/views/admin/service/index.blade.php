@extends('admin.layout.master')
@section('content')

    <h2 style="text-align: center;padding-bottom: 2%;">All Services</h2>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg" style="padding:10px;">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">Add Service</div>
                            <div class="card-body">
                                <form method="POST" action="{{route('service.store')}}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="mb-3">
                                        <label for="title" class="form-label">Title</label>
                                        <input type="text" class="form-control" id="title" name="title" aria-describedby="title">
                                        @error('title')
                                        <div style="color:red;">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <label for="short_des" class="form-label">Description</label>
                                        <textarea type="textarea" class="form-control" rows="3" cols="3" id="short_des" name="short_des" aria-describedby="short_des"></textarea>
                                        @error('short_des')
                                        <div style="color:red;">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <label for="image" class="form-label">Image</label>
                                        <input type="file" class="form-control" id="image" name="image" aria-describedby="image">
                                        @error('image')
                                        <div style="color:red;">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <button type="submit" class="btn btn-success">Submit</button>

                                </form>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">All Services</div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Serial No</th>
                                        <th scope="col">Title</th>
                                        <th scope="col">Description</th>
                                        <th scope="col">Image</th>
                                        <th scope="col">Created_at</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($services)
                                        @foreach($services as $service)
                                            <tr>
                                                <th scope="row">{{$services->firstItem()+$loop->index}}</th>
                                                <td>{{ $service->title}}</td>
                                                <td>{{ Str::limit($service->short_des,20)}}</td>
                                                <td><img src="{{ asset($service->image) }}" style="height: 60px;width:70px;"></td>
                                                <td>{{ $service->created_at->diffForHumans()}}</td>
                                                <td>
                                                    <a href="{{route('service.edit',$service->id)}}" class="btn btn-small btn-info">Edit</a>
                                                    <a href="{{route('service.delete',$service->id)}}" onclick="return confirm('Are you sure to delete?')" class="btn btn-small btn-danger">Delete</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <h4>Currently there are no service available</h4>
                                    @endif

                                    </tbody>
                                </table>
                                {{$services->render()}}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
