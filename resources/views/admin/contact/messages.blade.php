@extends('admin.layout.master')
@section('content')

    <h2 style="text-align: center;padding-bottom: 2%;">All Messages</h2>
    <div class="row" >
            <div class="col-md-12" style="padding:2%;">
                <table class="table" style="width:100%;background-color: white">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Address</th>
                        <th scope="col">Email</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Description</th>
                        <th scope="col">Created_at</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($messages)
                        @foreach($messages as $message)
                            <tr>
                                <th scope="row">{{$messages->firstItem()+$loop->index}}</th>
                                <td>{{ $message->name}}</td>
                                <td>{{ $message->email}}</td>
                                <td style="width:10%;">{{ $message->subject}}</td>
                                <td style="width:30%;">{{ $message->description}}</td>
                                <td>{{ $message->created_at->diffForHumans()}}</td>
                                <td>
                                    @if($message->status == 1)
                                            <a href="{{route('message.approve',$message->id)}}" class="btn btn-small btn-outline-success">Approve</a>
                                    @else
                                        <a href="{{route('message.disapprove',$message->id)}}" class="btn btn-small btn-info">Approve</a>
                                    @endif
                                        <a href="{{route('message.delete',$message->id)}}" onclick="return confirm('Are you sure to delete?')" class="btn btn-small btn-danger">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <h4>Currently there are no messages available</h4>
                    @endif

                    </tbody>
                </table>
                {{$messages->render()}}
            </div>
        </div>






@endsection



