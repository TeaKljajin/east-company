@extends('admin.layout.master')
@section('content')


    <h2 style="text-align: center;padding-bottom: 2%;">Profile Information</h2>
    <div class="row">
        <div class="col-md-8" style="margin-left: 20%">
            <div class="card">
                <div class="card-header">Profile
                    @if($contact)
                    <a href="{{route('contact.edit',$contact->id)}}" style="float:right;width:80px;"  class="btn btn-small btn-info">Edit</a>
                        <a href="{{route('contact.delete',$contact->id)}}" style="float:right;margin-right: 10px;" onclick="return confirm('Are you sure to delete?')" class="btn btn-small btn-danger">Delete</a>

                    @endif

                </div>
                @if($contact)
                    <div class="card-body">
                        <h5>Address: {{$contact->name}}</h5><br>
                        <h5>Address: {{$contact->address}}</h5><br>
                        <h5>Address: {{$contact->city}}</h5><br>
                        <h5>Email: {{$contact->email}}</h5><br>
                        <h5>Phone: {{$contact->phone}}</h5><br>
                    </div>
                @else
                    <div class="card-body">
                        <form method="POST" action="{{route('contact.store')}}">
                            @csrf
                            <div class="mb-3">
                                <label for="name" class="form-label">Website Name</label>
                                <input type="text" class="form-control" id="name" name="name" aria-describedby="name">
                                @error('name')
                                <div style="color:red;">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="address" class="form-label">Address</label>
                                <input type="text" class="form-control" id="address" name="address" aria-describedby="address">
                                @error('address')
                                <div style="color:red;">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="city" class="form-label">City</label>
                                <input type="text" class="form-control" id="city" name="city" aria-describedby="city">
                                @error('city')
                                <div style="color:red;">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="email" class="form-label">Email</label>
                                <input type="text" class="form-control" id="email" name="email" aria-describedby="email">
                                @error('email')
                                <div style="color:red;">{{ $message }}</div>
                                @enderror

                            </div>
                            <div class="mb-3">
                                <label for="phone" class="form-label">Phone</label>
                                <input type="number" class="form-control" id="phone" name="phone" aria-describedby="phone">
                                @error('phone')
                                <div style="color:red;">{{ $message }}</div>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-success">Submit</button>

                        </form>
                    </div>

                @endif

            </div>
        </div>






@endsection


