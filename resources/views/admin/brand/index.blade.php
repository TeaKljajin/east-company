@extends('admin.layout.master')
@section('content')


    <h2 style="text-align: center;padding-bottom: 2%;">All Clients</h2>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg" style="padding:10px;">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">Add Client</div>
                            <div class="card-body">
                                <form method="POST" action="{{route('brand.store')}}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="mb-3">
                                        <label for="brand_name" class="form-label">Name</label>
                                        <input type="text" class="form-control" id="brand_name" name="brand_name" aria-describedby="brand_name"
                                               value="Add New Brand" style="opacity: 0.7" onfocus="this.value=''">
                                        @error('brand_name')
                                        <div style="color:red;">{{ $message }}</div>
                                        @enderror
                                        @if(session('errorMessage'))
                                            <div style="color:red;">{{ session('errorMessage') }}</div>
                                        @endif
                                    </div>
                                    <div class="mb-3">
                                        <label for="brand_image" class="form-label">Image</label>
                                        <input type="file" class="form-control" id="brand_image" name="brand_image" aria-describedby="brand_image">
                                        @error('brand_image')
                                        <div style="color:red;">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <button type="submit" class="btn btn-success">Submit</button>

                                </form>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">All Clients</div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Serial No</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Image</th>
                                        <th scope="col">Created_at</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($brands)
                                        @foreach($brands as $brand)
                                            <tr>
                                                <th scope="row">{{$brands->firstItem()+$loop->index}}</th>
                                                <td>{{ $brand->brand_name}}</td>
                                                <td><img src="{{ asset($brand->brand_image) }}" style="height: 60px;width:70px;"></td>
                                                <td>{{ $brand->created_at->diffForHumans()}}</td>
                                                <td>
                                                    <a href="{{route('brand.edit',$brand->id)}}" class="btn btn-small btn-info">Edit</a>
                                                    <a href="{{route('brand.delete',$brand->id)}}" onclick="return confirm('Are you sure to delete?')" class="btn btn-small btn-danger">Delete</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <h4>Currently there are no brand available</h4>
                                    @endif

                                    </tbody>
                                </table>
                                {{$brands->render()}}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
