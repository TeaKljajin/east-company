@extends('admin.layout.master')
@section('content')
    <h2 style="text-align: center;padding-bottom: 2%;">All Trashed Brands</h2>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg" style="padding:10px;">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">Brands Trash List</div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Serial No</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Image</th>
                                        <th scope="col">Created_at</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                    </thead>
                                    @if($trashBrands)
                                        <tbody>
                                        @foreach($trashBrands as $brand)
                                            <tr>
                                                <th scope="row">{{$trashBrands->firstItem()+$loop->index}}</th>
                                                <td>{{$brand->brand_name}}</td>
                                                <td><img src="{{asset($brand->brand_image)}}" style="height:60px;width:70px;"></td>
                                                <td>{{ $brand->created_at->diffForHumans()}}</td>
                                                <td>
                                                    <a href="{{route('brand.permanentlyDeleted',$brand->id)}}" onclick="return confirm('After this action you will not be in position to restore your data!')" class="btn btn-small btn-danger" >Permanent Delete</a>
                                                    <a href="{{route('brand.restore',$brand->id)}}" class="btn btn-small btn-success">Restore</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    @else
                                        <h4>Currently there are no trashed brands</h4>
                                    @endif
                                </table>
                                {{$trashBrands->render()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


