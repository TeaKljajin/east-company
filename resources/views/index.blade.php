@extends('front.layout.master')
@section('content')
    <!-- ======= About Us Section ======= -->
        @include('front.content.about')
    <!-- End About Us Section -->

    <!-- ======= Services Section ======= -->
        @include('front.content.services')
    <!-- End Services Section -->

    <!-- ======= Portfolio Section ======= -->
        @include('front.content.portfolio')
    <!-- End Portfolio Section -->

    <!-- ======= Our Clients Section ======= -->
        @include('front.content.clients')
    <!-- End Our Clients Section -->

@endsection
