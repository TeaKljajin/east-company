@extends('front.layout.master')
@section('content')
    <div class="mt-5">
        <!-- ======= Portfolio Section ======= -->
    @include('front.content.services')
    <!-- End Portfolio Section -->
    </div>
    <div class="section-title mt-5">
        <h2>Clients</h2>
        <p>Laborum repudiandae omnis voluptatum consequatur mollitia ea est voluptas ut</p>
    </div>
    <div class="container mb-5">
        <div class="row">
            @foreach($brands as $brand)
                <div class="col-lg-3 col-md-4 col-6" style="border:1px solid grey">
                    <div style="padding:15px;">
                        <img src="{{$brand->brand_image}}" class="img-fluid" style="height:80px;" alt="">
                    </div>
                </div>
            @endforeach
        </div>
    </div>


@endsection
