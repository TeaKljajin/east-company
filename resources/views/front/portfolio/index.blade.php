@extends('front.layout.master')
@section('content')
<div class="mt-5">
    <!-- ======= Portfolio Section ======= -->
    @include('front.content.portfolio')
    <!-- End Portfolio Section -->
</div>

@endsection

