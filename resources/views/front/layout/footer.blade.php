@if(request()->routeIs('index') || request()->routeIs('about') || request()->routeIs('service') || request()->routeIs('portfolio') || request()->routeIs('contact'))

    <footer id="footer">
<div class="footer-top">
    <div class="container">
        <div class="row">

            <div class="col-lg-3 col-md-6 footer-contact">
                <h3>East Company</h3>
                <p>
                    {{$contact->address}}<br>
                    {{$contact->city}} <br><br>
                    <strong>Phone:</strong> {{$contact->phone}}<br>
                    <strong>Email:</strong> {{$contact->email}}<br>
                </p>
            </div>

            <div class="col-lg-2 col-md-6 footer-links">
                <h4>Useful Links</h4>
                <ul>
                    <li><i class="bx bx-chevron-right"></i> <a href="{{route('index')}}">Home</a></li>
                    <li><i class="bx bx-chevron-right"></i> <a href="{{route('about')}}">About us</a></li>
                    <li><i class="bx bx-chevron-right"></i> <a href="{{route('service')}}">Services</a></li>
                    <li><i class="bx bx-chevron-right"></i> <a href="{{route('portfolio')}}">Portfolio</a></li>
                    <li><i class="bx bx-chevron-right"></i> <a href="{{route('contact')}}">Contact</a></li>
                </ul>
            </div>

            <div class="col-lg-3 col-md-6 footer-links">
                <h4>Our Services</h4>
                <ul>
                    <li><i class="bx bx-chevron-right"></i> Web Design</li>
                    <li><i class="bx bx-chevron-right"></i> Web Development</li>
                    <li><i class="bx bx-chevron-right"></i> Product Management</li>
                    <li><i class="bx bx-chevron-right"></i> Marketing</li>
                    <li><i class="bx bx-chevron-right"></i> Graphic Design</li>
                </ul>
            </div>

            <div class="col-lg-4 col-md-6 footer-newsletter">
                <h4>Join Our Newsletter</h4>
                <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
                <form action="{{route('newsletter.store')}}" method="post">
                    @csrf
                    <input type="email" name="email"><input type="submit" value="Subscribe">
                </form>
                @error('email')
                <div style="color:red;background-color: whitesmoke">{{ $message }}</div>
                @enderror
                @if(session('message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{{session('message')}}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
            </div>

        </div>
    </div>
</div>

<div class="container d-md-flex py-4">

    <div class="mr-md-auto text-center text-md-left">
        <div class="copyright">
            &copy; Copyright <strong><span>{{$contact->name}}</span></strong>. All Rights Reserved
        </div>
        <div class="credits">
            Designed by <a href="https://bootstrapmade.com/">Tea Kljajin</a>
        </div>
    </div>
    <div class="social-links text-center text-md-right pt-3 pt-md-0">
        @if(request()->routeIs('index') || request()->routeIs('about') || request()->routeIs('service') || request()->routeIs('portfolio') || request()->routeIs('contact'))
            @if($social->twitter == 'null')
                @else
                <a href="{{$social->twitter}}" class="twitter"><i class="bx bxl-twitter"></i></a>
            @endif

            @if($social->facebook == 'null')
                @else
                <a href="{{$social->facebook}}" class="facebook"><i class="bx bxl-facebook"></i></a>
            @endif
            @if($social->instagram == 'null')
                @else
                <a href="{{$social->instagram}}" class="instagram"><i class="bx bxl-instagram"></i></a>
            @endif
            @if($social->pinterest == 'null')
                @else
                <a href="{{$social->pinterest}}" class="linkedin"><i class="bx bxl-pinterest"></i></a>
            @endif
        @endif
    </div>

</div>
</footer>
@endif
