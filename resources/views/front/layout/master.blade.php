@include('front.layout.head')

<body>

<!-- ======= Header ======= -->
@include('front.layout.header')
<!-- End Header -->
@if(request()->routeIs('portfolio') || request()->routeIs('about') || request()->routeIs('service') || request()->routeIs('contact') )

@else
    <!-- ======= Hero Section ======= -->
    @include('front.content.hero')
    <!-- End Hero -->

@endif

<main id="main">
     @yield('content')

</main><!-- End #main -->

<!-- ======= Footer ======= -->
@include('front.layout.footer')
<!-- End Footer -->

<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

@include('front.layout.scripts')
