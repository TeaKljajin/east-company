<header id="header" class="fixed-top">
<div class="container d-flex align-items-center">

    <h1 class="logo mr-auto"><a href="#"><span>{{$contact->name}}</span></a></h1>
    <!-- Uncomment below if you prefer to use an image logo -->
    <!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

    <nav class="nav-menu d-none d-lg-block">
        <ul>


            <li class="{{request()->routeIs('index') ? 'active' : ''}}"><a href="{{route('index')}}">Home</a></li>
            <li class="{{request()->routeIs('about') ? 'active' : ''}}"><a  href="{{route('about')}}">About</a></li>
            <li class="{{request()->routeIs('service') ? 'active' : ''}}"><a href="{{route('service')}}">Services</a></li>
            <li class="{{request()->routeIs('portfolio') ? 'active' : ''}}"><a href="{{route('portfolio')}}">Portfolio</a></li>
            <li class="{{request()->routeIs('contact') ? 'active' : ''}}"><a href="{{route('contact')}}">Contact</a></li>
            <li><a href="{{route('login')}}" >Login</a></li>

        </ul>
    </nav><!-- .nav-menu -->

    <div class="header-social-links">
        @if(request()->routeIs('index') || request()->routeIs('about') || request()->routeIs('service') || request()->routeIs('portfolio') || request()->routeIs('contact'))
            @if($social->twitter == 'null')
                @else
               <a href="{{$social->twitter}}" class="twitter"><i class="icofont-twitter"></i></a>
            @endif
            @if($social->facebook == 'null')
                @else
                 <a href="{{$social->facebook}}" class="facebook"><i class="icofont-facebook"></i></a>
                @endif
                @if($social->instagram == 'null')
                    @else
                  <a href="{{$social->instagram}}" class="instagram"><i class="icofont-instagram"></i></a>
                @endif
                @if($social->pinterest == 'null')
                    @else
                      <a href="{{$social->pinterest}}" class="linkedin"><i class="icofont-pinterest"></i></a>
                    @endif
        @endif
    </div>

</div>
</header>
