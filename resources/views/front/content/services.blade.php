@if(request()->routeIs('service'))
<!-- ======= Breadcrumbs ======= -->
<section id="breadcrumbs" class="breadcrumbs">
    <div class="container">

        <div class="d-flex justify-content-between align-items-center">
            <h2>Services</h2>
            <ol>
                <li><a href="{{route('index')}}">Home</a></li>
                <li>Services</li>
            </ol>
        </div>

    </div>
</section><!-- End Breadcrumbs -->
@endif


@if(count($services) > 0)
<section id="services" class="services section-bg">
<div class="container" data-aos="fade-up">
    @if(request()->routeIs('service'))

    @else
    <div class="section-title">
        <h2>Services</h2>
        <p>Laborum repudiandae omnis voluptatum consequatur mollitia ea est voluptas ut</p>
    </div>
    @endif

    <div class="row">
        @foreach($services as $service)
            <div class="col-lg-4" >
                <div style="margin: 10px;background-color: white;text-align: center;width:100%;height:330px;">
                <img src="{{asset($service->image)}}" style="width:140px;height:120px;margin-top: 50px;"><br><br>
                <h4 class="mt-1">{{$service->title}}</h4>
                <p>{{$service->short_des}}</p>
                </div>
            </div>
        @endforeach
    </div>

</div>
</section>
@endif
