@if(request()->routeIs('about'))
<!-- ======= Breadcrumbs ======= -->
<section id="breadcrumbs" class="breadcrumbs">
    <div class="container">

        <div class="d-flex justify-content-between align-items-center">
            <h2>About</h2>
            <ol>
                <li><a href="{{route('index')}}">Home</a></li>
                <li>About</li>
            </ol>
        </div>

    </div>
</section><!-- End Breadcrumbs -->
@endif

@if($about)
<section id="about-us" class="about-us">
<div  data-aos="fade-up">
<div class="container" style="padding-top:7%;padding-bottom: 7%">
    @if(request()->routeIs('about'))

    @else
        <div class="section-title">
            <h2>About Us</strong></h2>
        </div>
    @endif
    <div class="row content">
        <div class="col-lg-6" data-aos="fade-right">
            <h2>{{$about->title}}</h2>
            <h3>{{$about->short_des}}</h3>
        </div>
        <div class="col-lg-6 pt-4 pt-lg-0" data-aos="fade-left">
            <p>{{$about->long_des}}</p>
        </div>
    </div>
</div>
    <!-- ======= Our Team Section ======= -->
    @if(request()->routeIs('about'))
    @if(count($teams) > 0)

    <div class="container-fluid" style="background-color: whitesmoke">
        <div class="container" >
        <section id="team" class="team section-bg">
                <div class="section-title" data-aos="fade-up">
                    <h2>Our <strong>Team</strong></h2>
                    <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
                </div>
                <div class="row">
                    @foreach($teams as $team)
                    <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                        <div class="member" data-aos="fade-up">
                            <div class="member-img">
                                <img src="{{asset($team->image)}}" class="img-fluid" alt="">
                                <div class="social">
                                    <a href=""><i class="icofont-twitter"></i></a>
                                    <a href=""><i class="icofont-facebook"></i></a>
                                    <a href=""><i class="icofont-instagram"></i></a>
                                    <a href=""><i class="icofont-linkedin"></i></a>
                                </div>
                            </div>
                            <div class="member-info">
                                <h4>{{$team->name}}</h4>
                                <span>{{$team->position}}</span>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
        </section>
        </div>
    </div>

    @endif
        @endif
<!-- End Our Team Section -->


    <!-- ======= Testimonials ================ --->
    @if(request()->routeIs('about'))
        @if(count($teams) > 0)
        <!-- ======= Testimonials Section ======= -->
            <div class="container-fluid" style="background-color: #FFFFFF!important;">
            <section id="testimonials" class="testimonials">
                <div class="container">
                    <h2 style="text-align: center"><sctrong>Testimonials</sctrong></h2>
                    <div class="row">
                        @foreach($testimonials as $testimonial)
                        <div class="col-lg-6 mt-4" data-aos="fade-up">
                            <div class="testimonial-item">
                                <img src="{{asset($testimonial->image)}}" class="testimonial-img" alt="">
                                <h3>{{asset($testimonial->name)}}</h3>
                                <h4>{{asset($testimonial->position)}}</h4>
                                <p>
                                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    {{asset($testimonial->description)}}
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                        @endforeach
                    </div>

                </div>
            </section><!-- End Testimonials Section -->
            </div>
    @endif
        @endif


</div>
</section>
@endif
