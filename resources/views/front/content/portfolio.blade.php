@if(request()->routeIs('portfolio'))
<!-- ======= Breadcrumbs ======= -->
<section id="breadcrumbs" class="breadcrumbs">
    <div class="container">

        <div class="d-flex justify-content-between align-items-center">
            <h2>Portfolio</h2>
            <ol>
                <li><a href="{{route('index')}}">Home</a></li>
                <li>Portfolio</li>
            </ol>
        </div>

    </div>
</section><!-- End Breadcrumbs -->
@endif

@if(count($portfolios) > 0)
<section id="portfolio" class="portfolio">
<div class="container">
        @if(request()->routeIs('portfolio'))

        @else
        <div class="section-title" data-aos="fade-up">
            <h2>Portfolio</h2>
        </div>
        @endif



    <div class="row portfolio-container" data-aos="fade-up">
        @foreach($portfolios as $portfolio)
        <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <img src="{{asset($portfolio->images)}}" class="img-fluid" alt="Image" style="width:300px;height:250px;">
            <div class="portfolio-info">
                <h4>App 1</h4>
                <p>App</p>
                <a href="{{asset($portfolio->images)}}" data-gall="portfolioGallery" class="venobox preview-link" title="App 1" ><i class="bx bx-plus"></i></a>
                <a href="#" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
        </div>
        @endforeach

    </div>

</div>
</section>
@endif
