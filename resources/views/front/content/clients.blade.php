@if(count($brands) > 0)
<section id="clients" class="clients">
<div class="container" data-aos="fade-up">

    <div class="section-title">
        <h2>Clients</h2>
    </div>

    <div class="row no-gutters clients-wrap clearfix" style="border:none;" data-aos="fade-up">
         @foreach($brands as $brand)
        <div class="col-lg-3 col-md-4 col-6" style="border:1px solid grey">
            <div style="padding:10px;">
                <img src="{{$brand->brand_image}}" class="img-fluid" style="height:80px;" alt="">
            </div>
        </div>
        @endforeach

    </div>

</div>
</section>
@endif
