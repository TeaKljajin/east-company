@extends('admin.layout.master')
@section('content')
    @include('admin.content.topStatistics')
    @include('admin.content.salesOfTheYear')
    @include('admin.content.polarRadarChartSalesAndNotification')
    @include('admin.content.recentOrders')
    @include('admin.content.toDoListWorldChart')
    @include('admin.content.newCustomerTopProducts')
@endsection
