<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

class MessageController extends Controller
{

    public function __construct()
    {
        return $this->middleware('auth');
    }


    public function index(){
        $messages = Message::latest()->paginate(5);
        return view('admin.contact.messages',compact('messages'));
    }

    public function store(Request  $request){

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'subject' => 'required|max:255',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->route('contact')
                ->withErrors($validator)
                ->withInput();
        } else {

            Message::insert([
                'name' => $request->name,
                'email' => $request->email,
                'subject' => $request->subject,
                'description' => $request->description,
                'status' => 1,
                'created_at' => Carbon::now(),
            ]);
        }
        //Session message
        $toaster = array(
            'message' => 'Your message has been successfully sent',
            'alert-type' => 'success'
        );
        //Redirect to
        return redirect()->route('contact')->with($toaster);

    }

    public function delete($id)
    {
        $message = Message::find($id);
        $message->delete();
        //Session message
        $toaster = array(
            'message' => 'You deleted successfully message',
            'alert-type' => 'warning'
        );
        //Redirect to
        return redirect()->route('message.index')->with($toaster);

    }



    public function approve($id){
        $message = Message::find($id);
        $message->update([
            'status' => 2
        ]);
        //Session message
        $toaster = array(
            'message' => 'You approved successfully message',
            'alert-type' => 'success'
        );

        //Redirect to
        return redirect()->route('message.index')->with($toaster);
    }

    public function disApprove($id){
        $message = Message::find($id);
        $message->update([
            'status' => 1
        ]);
        //Session message
        $toaster = array(
            'message' => 'You dis approved successfully message',
            'alert-type' => 'warning'
        );

        //Redirect to
        return redirect()->route('message.index')->with($toaster);
    }
}
