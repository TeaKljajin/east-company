<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Models\Team;

class TeamController extends Controller
{


    public function __construct()
    {
        return $this->middleware('auth');
    }


    public function index()
    {
        $teams = Team::latest()->paginate(5);;

        return view('admin.team.index', compact('teams'));
    }


    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:teams|min:2|max:255',
            'position' => 'required|min:2|max:255',
            'image' => 'required|mimes:jpg,jpeg,png',

        ]);

        if ($validator->fails()) {
            return redirect()->route('team.index')
                ->withErrors($validator)
                ->withInput();
        } elseif ($request->brand_name === 'Add New Team') {
            return redirect()->route('team.index')->with('errorMessage', 'You must type new name');
        } else {
            //Prepare Image
            $image = $request->file('image');
            $brand_ext = strtolower($image->getClientOriginalExtension());
            $name_gen = hexdec(uniqid());
            $up_location = "images/team/";
            $img_name = $name_gen . '.' . $brand_ext;
            $image->move($up_location, $img_name);
            $name_for_db = $up_location . $img_name;

            Team::insert([
                'name' => $request->name,
                'position' => $request->position,
                'image' => $name_for_db,
                'created_at' => Carbon::now(),
            ]);
            $toaster = array(
                'message' => 'You created successfully new team member',
                'alert-type' => 'success'
            );
            return redirect()->route('team.index')->with($toaster);
        }

    }


    public function edit($id)
    {
        $teams = Team::latest()->paginate(5);
        $team = Team::find($id);
        return view('admin.team.edit', compact('teams', 'team'));

    }


    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:teams|min:2|max:255',
            'position' => 'required|min:2|max:255',
            'image' => 'required|mimes:jpg,jpeg,png',

        ]);

        if ($validator->fails()) {
            return redirect()->route('team.edit', $request->id)
                ->withErrors($validator)
                ->withInput();
        } else {

            //Find Old Image
            $old_image = Team::find($id);
            $old_image = $old_image->image;

            //New Image
            $image = $request->file('image');

            if ($image) {
                $brand_ext = strtolower($image->getClientOriginalExtension());
                $name_gen = hexdec(uniqid());
                $up_location = "images/team/";
                $img_name = $name_gen . '.' . $brand_ext;
                $image->move($up_location, $img_name);
                $name_for_db = $up_location . $img_name;

                unlink($old_image);
                Team::find($id)->update([
                    'name' => $request->name,
                    'position' => $request->position,
                    'image' => $name_for_db,
                    'created_at' => Carbon::now(),
                ]);

                session()->flash('message', 'You updated successfully team member');
                return redirect()->route('team.index');

            } else {
                Team::find($id)->update([
                    'name' => $request->name,
                    'position' => $request->position,
                    'created_at' => Carbon::now(),
                ]);
                $toaster = array(
                    'message' => 'You updated successfully new team member',
                    'alert-type' => 'info'
                );
                return redirect()->route('team.index')->with($toaster);


            }

        }

    }


    public function delete($id)
    {
        $team = Team::find($id);
        $image = $team->image;
        if($image){
            unlink($image);
        }
        $team->delete();
        $toaster = array(
            'message' => 'You deleted successfully new team member',
            'alert-type' => 'warning'
        );
        return redirect()->route('team.index')->with($toaster);

    }





}
