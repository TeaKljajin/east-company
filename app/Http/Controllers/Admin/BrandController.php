<?php

namespace App\Http\Controllers\Admin;

    use App\Http\Controllers\Controller;
    use App\Models\Brand;
    use Illuminate\Http\Request;
    use Illuminate\Support\Carbon;
    use Illuminate\Support\Facades\Validator;

class BrandController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }


    public function index()
    {
        $brands = Brand::latest()->paginate(5);;

        return view('admin.brand.index',compact('brands'));
    }


    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'brand_name' => 'required|unique:brands|min:2|max:255',
            'brand_image' => 'required|mimes:jpg,jpeg,png',

        ]);

        if ($validator->fails()) {
            return redirect()->route('brand.index')
                ->withErrors($validator)
                ->withInput();
        } elseif ($request->brand_name === 'Add New Brand') {
            return redirect()->route('brand.index')->with('errorMessage', 'You must type new name');
        } else {
            //Prepare Image
            $brand_image = $request->file('brand_image');
            $brand_ext = strtolower($brand_image->getClientOriginalExtension());
            $name_gen = hexdec(uniqid());
            $up_location = "images/brand/";
            $img_name = $name_gen . '.' . $brand_ext;
            $brand_image->move($up_location, $img_name);
            $name_for_db = $up_location . $img_name;

            Brand::insert([
                'brand_name' => $request->brand_name,
                'brand_image' => $name_for_db,
                'created_at' => Carbon::now(),
            ]);

            $toaster = array(
                'message' => 'You created successfully new brand',
                'alert-type' => 'success'
            );

            return redirect()->route('brand.index')->with($toaster);
        }

    }


    public function edit($id)
    {
        $brands = Brand::latest()->paginate(5);
        $brand = Brand::find($id);
        $trashBrands = Brand::onlyTrashed()->latest()->paginate(5);
        return view('admin.brand.edit',compact('brands','trashBrands','brand'));

    }


    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'brand_name' => 'required|min:2|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->route('brand.edit', $request->id)
                ->withErrors($validator)
                ->withInput();
        } else {

            //Find Old Image
            $old_image = $request->old_image;

            //New Image
            $brand_image = $request->file('brand_image');

            if ($brand_image) {
                $brand_ext = strtolower($brand_image->getClientOriginalExtension());
                $name_gen = hexdec(uniqid());
                $up_location = "images/brand/";
                $img_name = $name_gen . '.' . $brand_ext;
                $brand_image->move($up_location, $img_name);
                $name_for_db = $up_location . $img_name;

                unlink($old_image);
                Brand::find($id)->update([
                    'brand_name' => $request->brand_name,
                    'brand_image' => $name_for_db,
                    'created_at' => Carbon::now(),
                ]);
                session()->flash('message', 'You updated successfully brand');
                return redirect()->route('brand.index');

            } else {
                Brand::find($id)->update([
                    'brand_name' => $request->brand_name,
                    'created_at' => Carbon::now(),
                ]);
                $toaster = array(
                    'message' => 'You updated successfully brand',
                    'alert-type' => 'info'
                );

                return redirect()->route('brand.index')->with($toaster);


            }

        }

    }


    public function delete($id)
    {
        $brand = Brand::find($id);
        $brand->delete();
        $toaster = array(
            'message' => 'You deleted successfully brand',
            'alert-type' => 'warning'
        );

        return redirect()->route('brand.index')->with($toaster);

    }

    public function restore($id)
    {
        $brand = Brand::withTrashed()->find($id)->restore();
        $toaster = array(
            'message' => 'You restored successfully brand',
            'alert-type' => 'success'
        );

        return redirect()->route('brand.trashed')->with($toaster);

    }

    public function permanentlyDeleted($id)
    {
        $brand = Brand::onlyTrashed()->find($id);
        $image = $brand->brand_image;
        unlink($image);

        Brand::onlyTrashed()->find($id)->forceDelete();
        $toaster = array(
            'message' => 'You deleted permanently brand',
            'alert-type' => 'warning'
        );

        return redirect()->route('brand.trashed')->with($toaster);


    }

    public function trashed(){
        $trashBrands = Brand::onlyTrashed()->latest()->paginate(5);
        return view('admin.brand.trashed',compact('trashBrands'));
    }



}
