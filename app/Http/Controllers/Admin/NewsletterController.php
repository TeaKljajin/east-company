<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Newsletter;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

class NewsletterController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }

    public function index(){
        $newsletters = Newsletter::latest()->paginate(5);
        return view('admin.newsletter.index',compact('newsletters'));
    }
    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255',

        ]);

        if ($validator->fails()) {
            return redirect()->route('index')
                ->withErrors($validator)
                ->withInput();
        } else {

            Newsletter::insert([
                'email' => $request->email,
                'created_at' => Carbon::now(),
            ]);
        }
        $toaster = array(
            'message' => 'Your successfully subscribe to our newsletter',
            'alert-type' => 'success'
        );
        return redirect()->route('index')->with($toaster);

    }

    public function delete($id)
    {
        $newsletter = Newsletter::find($id);
        $newsletter->delete();
        //Session message
        $toaster = array(
            'message' => 'You deleted successfully newsletter',
            'alert-type' => 'warning'
        );

        return redirect()->route('newsletter.index')->with($toaster);

    }

}
