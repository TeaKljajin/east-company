<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Slider;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

class SliderController extends Controller
{

    public function __construct()
    {
        return $this->middleware('auth');
    }

    public function index(){
        $sliders = Slider::latest()->paginate(5);
        return view('admin.slider.index',compact('sliders'));
    }


    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'description' => 'nullable|max:255',
            'image' => 'required|mimes:jpg,jpeg,png',

        ]);

        if ($validator->fails()) {
            return redirect()->route('slider.index')
                ->withErrors($validator)
                ->withInput();
        } elseif ($request->title === 'Add New Title') {
            return redirect()->route('slider.index')->with('errorMessage', 'You must type new name');
        } else {
            //Prepare Image
            if(!empty($request->file('image'))){
                $image = $request->file('image');
                $brand_ext = strtolower($image->getClientOriginalExtension());
                $name_gen = hexdec(uniqid());
                $up_location = "images/slider/";
                $img_name = $name_gen . '.' . $brand_ext;
                $image->move($up_location, $img_name);
                $name_for_db = $up_location . $img_name;
                Slider::insert([
                    'title' => $request->title,
                    'description' => $request->description,
                    'image' => $name_for_db,
                    'created_at' => Carbon::now(),
                ]);

            }else{
                Slider::insert([
                    'title' => $request->title,
                    'description' => $request->description,
                    'created_at' => Carbon::now(),
                ]);
            }
            $toaster = array(
                'message' => 'You created successfully new slider',
                'alert-type' => 'success'
            );
            return redirect()->route('slider.index')->with($toaster);
        }

    }


    public function edit($id)
    {
        $sliders = Slider::latest()->paginate(15);
        $slider = Slider::find($id);
        return view('admin.slider.edit',compact('sliders','slider'));

    }


    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'max:255',
            'image' => 'mimes:jpg,jpeg,png',
        ]);

        if ($validator->fails()) {
            return redirect()->route('slider.edit', $request->id)
                ->withErrors($validator)
                ->withInput();
        } else {

            //Find Old Image
            $old_image = $request->old_image;

            //New Image
            $image = $request->file('image');

            if ($image) {
                $brand_ext = strtolower($image->getClientOriginalExtension());
                $name_gen = hexdec(uniqid());
                $up_location = "images/slider/";
                $img_name = $name_gen . '.' . $brand_ext;
                $image->move($up_location, $img_name);
                $name_for_db = $up_location . $img_name;

                unlink($old_image);
                Slider::find($id)->update([
                    'title' => $request->title,
                    'description' => $request->description,
                    'image' => $name_for_db,
                    'created_at' => Carbon::now(),
                ]);
                session()->flash('message', 'You updated successfully slider');
                return redirect()->route('slider.index');

            } else {
                Slider::find($id)->update([
                    'title' => $request->title,
                    'description' => $request->description,
                    'created_at' => Carbon::now(),
                ]);
                $toaster = array(
                    'message' => 'You updated successfully new slider',
                    'alert-type' => 'info'
                );
                return redirect()->route('slider.index')->with($toaster);


            }

        }

    }


    public function delete($id)
    {
        $slider = Slider::find($id);
        $image = $slider->image;
        if($image){
            unlink($image);
        }
        $slider->delete();
        $toaster = array(
            'message' => 'You deleted successfully slider',
            'alert-type' => 'warning'
        );
        return redirect()->route('slider.index')->with($toaster);

    }






}
