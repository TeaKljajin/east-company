<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\About;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;


class AboutController extends Controller
{

    public function __construct()
    {
        return $this->middleware('auth');
    }


    public function index(){
        $abouts = About::paginate(6);
        return view('admin.about.index',compact('abouts'));
    }

    public function edit($id){
        $abouts = About::latest()->paginate(5);
        $about = about::find($id);
        return view('admin.about.edit',compact('about','abouts'));
    }

    public function store(Request  $request){

        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'short_des' => 'required|max:255',
            'long_des' => 'required',


        ]);

        if ($validator->fails()) {
            return redirect()->route('about.index')
                ->withErrors($validator)
                ->withInput();
        } else {

                About::insert([
                    'title' => $request->title,
                    'short_des' => $request->short_des,
                    'long_des' => $request->long_des,
                    'created_at' => Carbon::now(),
                ]);
            }

             $toaster = array(
                 'message' => 'You created successfully new about page text',
                 'alert-type' => 'success'
             );

            return redirect()->route('about.index')->with($toaster);

    }


    public function update(Request $request, $id){
        $about = About::find($id);
        $validator = Validator::make($request->all(), [
            'title' => 'max:255',
            'short_des' => 'max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->route('about.index')
                ->withErrors($validator)
                ->withInput();
        } else {
            $about->update([
                'title' => $request->title,
                'short_des' => $request->short_des,
                'long_des' => $request->long_des,
                'created_at' => Carbon::now(),
            ]);
        }

        $toaster = array(
            'message' => 'You updated successfully new about page text',
            'alert-type' => 'info'
        );

        return redirect()->route('about.index')->with($toaster);

    }


    public function delete($id){
        $about = About::find($id);
        $about->delete();
        $toaster = array(
            'message' => 'You deleted successfully about page text',
            'alert-type' => 'warning'
        );
        return redirect()->route('about.index')->with($toaster);
    }


}
