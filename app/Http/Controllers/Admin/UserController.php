<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SocialMedia;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    public function __construct()
    {
        return $this->middleware('auth');
    }

    public function index(){
        $social = SocialMedia::first();
        $user = Auth::user();
        return view('admin.profile.index',compact('social','user'));
    }

    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'name' => 'min:2|max:255',
            'email' => 'email',
            'image' => 'mimes:jpg,jpeg,png',
        ]);

        if ($validator->fails()) {
            return redirect()->route('profile.index')
                ->withErrors($validator)
                ->withInput();
        } else {

            //Find Old Image
            $user = User::find($id);
            $user_image = $user->profile_photo_path;

            //New Image
             $image = $request->image;

            if ($image) {
                $brand_ext = strtolower($image->getClientOriginalExtension());
                $name_gen = hexdec(uniqid());
                $up_location = "images/user/";
                $img_name = $name_gen . '.' . $brand_ext;
                $image->move($up_location, $img_name);
                $name_for_db = $up_location . $img_name;
                if($user_image){
                    unlink($user_image);
                }
                $user->update([
                    'name' => $request->name,
                    'profile_photo_path' => $name_for_db,
                    'updated_at' => Carbon::now(),
                ]);
                $toaster = array(
                    'message' => 'You updated successfully admin profile',
                    'alert-type' => 'info'
                );
                return redirect()->route('profile.index')->with($toaster);

            } else {
                $user->update([
                    'name' => $request->name,
                    'email' => $request->email,
                    'updated_at' => Carbon::now(),
                ]);
                $toaster = array(
                    'message' => 'You updated successfully admin profile',
                    'alert-type' => 'info'
                );
                return redirect()->route('profile.index')->with($toaster);
            }
        }


    }


    public function change_password(){
        $user = User::first();
        return view('admin.profile.change_password',compact('user'));
    }


    public function changePassword(Request $request, $id){

        //Find the user
        $user = User::findOrFail($id);

        //Validate data
        $this->validate($request,[
            'password'=>'required',
            'new_password'=>'required',
            'password_confirmation' => 'required',
        ]);
        //Check the password input

            if (!(Hash::check($request->get('password'), Auth::user()->password))) {
                // The passwords not matches
                return redirect()->back()->with("old","Your current password does not matches with the password you provided. Please try again.");
            }
            if(strcmp($request->get('password'), $request->get('new_password')) == 0){
                //Current password and new password are same
                return redirect()->back()->with("new","New Password cannot be same as your current password. Please choose a different password.");
            }
            if(strcmp($request->get('new_password'), $request->get('password_confirmation')) !== 0){
                //Confirmation password and new password are not same
                return redirect()->back()->with("conf","Password Confirmation is not same as your new password. Please type the same password.");
            }
            $user->password = bcrypt($request['new_password']);
            //Save data to database
            $user->update([
                'password' => $user->password,
            ]);


        //Session message
        $toaster = array(
            'message' => 'Password was successfully changed',
            'alert-type' => 'success'
        );

        //Redirect to
        return redirect()->route('profile.change.password')->with($toaster);

    }


}
