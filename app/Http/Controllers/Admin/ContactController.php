<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{

    public function __construct()
    {
        return $this->middleware('auth');
    }


    public function index(){
        $contact = Contact::first();
        return view('admin.contact.index',compact('contact'));
    }

    public function store(Request  $request){

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'address' => 'required|max:255',
            'city' => 'required|max:255',
            'email' => 'required|max:255',
            'phone' => 'required|min:9|max:20',


        ]);

        if ($validator->fails()) {
            return redirect()->route('contact.index')
                ->withErrors($validator)
                ->withInput();
        } else {

            Contact::insert([
                'name' => $request->name,
                'address' => $request->address,
                'city' => $request->city,
                'email' => $request->email,
                'phone' => $request->phone,
                'created_at' => Carbon::now(),
            ]);
        }
        $toaster = array(
            'message' => 'You created successfully new contact profile',
            'alert-type' => 'success'
        );

        return redirect()->route('contact.index')->with($toaster);

    }

    public function edit($id){
        $contact = Contact::first();
        return view('admin.contact.edit',compact('contact'));
    }


    public function delete($id)
    {
        $contact = Contact::find($id);
        $contact->delete();
        $toaster = array(
            'message' => 'You deleted successfully contact profile',
            'alert-type' => 'warning'
        );
        return redirect()->route('contact.index')->with($toaster);

    }



}
