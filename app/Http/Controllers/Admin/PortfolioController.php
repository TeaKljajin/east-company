<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Models\Portfolio;
use Intervention\Image\Facades\Image;

//use Intervention\Image\Image;

class PortfolioController extends Controller
{

    public function __construct()
    {
        return $this->middleware('auth');
    }


    public function index(){
        $portfolios = Portfolio::latest()->paginate(9);
        return view('admin.portfolio.index',compact('portfolios'));
    }



    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'images' => 'required',

        ]);

        if ($validator->fails()) {
            return redirect()->route('portfolio.index')
                ->withErrors($validator)
                ->withInput();
        } else{
            //Prepare Image
            $images = $request->file('images');
            foreach($images as $image){
                $image_ext = strtolower($image->getClientOriginalExtension());
                $name_gen = hexdec(uniqid());
                $up_location = "images/portfolio/";
                $img_name = $name_gen. '.' .$image_ext;
                Image::make($image)->resize(800,800)->save($up_location.$img_name);

                $name_for_db = $up_location.$img_name;

                Portfolio::insert([
                    'images' => $name_for_db,
                    'created_at' => Carbon::now(),
                ]);

            }
            $toaster = array(
                'message' => 'You created successfully portfolio',
                'alert-type' => 'success'
            );

            return redirect()->route('portfolio.index')->with($toaster);
        }

    }


    public function delete($id){
        $portfolio = Portfolio::find($id);
        $image = $portfolio->images;
        unlink($image);

        $portfolio->delete();
        $toaster = array(
            'message' => 'You deleted portfolio successfully',
            'alert-type' => 'warning'
        );
        return redirect()->route('portfolio.index')->with($toaster);
    }



}
