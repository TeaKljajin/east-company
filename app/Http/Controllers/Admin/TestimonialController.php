<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Testimonial;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

class TestimonialController extends Controller
{

    public function __construct()
    {
        return $this->middleware('auth');
    }


    public function index()
    {
        $testimonials = Testimonial::latest()->paginate(5);;

        return view('admin.testimonial.index', compact('testimonials'));
    }


    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:teams|min:2|max:255',
            'position' => 'required|min:2|max:255',
            'description' => 'required|max:255',
            'image' => 'required|mimes:jpg,jpeg,png',

        ]);

        if ($validator->fails()) {
            return redirect()->route('testimonial.index')
                ->withErrors($validator)
                ->withInput();
        } elseif ($request->brand_name === 'Add New Testimonial') {
            return redirect()->route('testimonial.index')->with('errorMessage', 'You must type new name');
        } else {
            //Prepare Image
            $image = $request->file('image');
            $brand_ext = strtolower($image->getClientOriginalExtension());
            $name_gen = hexdec(uniqid());
            $up_location = "images/testimonial/";
            $img_name = $name_gen . '.' . $brand_ext;
            $image->move($up_location, $img_name);
            $name_for_db = $up_location . $img_name;

            Testimonial::insert([
                'name' => $request->name,
                'position' => $request->position,
                'description' => $request->description,
                'image' => $name_for_db,
                'created_at' => Carbon::now(),
            ]);
            $toaster = array(
                'message' => 'You created successfully new testimonial',
                'alert-type' => 'success'
            );
            return redirect()->route('testimonial.index')->with($toaster);
        }

    }


    public function edit($id)
    {
        $testimonials = Testimonial::latest()->paginate(5);
        $testimonial = Testimonial::find($id);
        return view('admin.testimonial.edit', compact('testimonials', 'testimonial'));

    }


    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'min:2|max:255',
            'position' => 'min:2|max:255',
            'description' => 'max:255',
            'image' => 'mimes:jpg,jpeg,png',

        ]);

        if ($validator->fails()) {
            return redirect()->route('testimonial.edit', $request->id)
                ->withErrors($validator)
                ->withInput();
        } else {

            //Find Old Image
            $old_image = Testimonial::find($id);
            $old_image = $old_image->image;

            //New Image
            $image = $request->file('image');

            if ($image) {
                $brand_ext = strtolower($image->getClientOriginalExtension());
                $name_gen = hexdec(uniqid());
                $up_location = "images/testimonial/";
                $img_name = $name_gen . '.' . $brand_ext;
                $image->move($up_location, $img_name);
                $name_for_db = $up_location . $img_name;

                unlink($old_image);
                Testimonial::find($id)->update([
                    'name' => $request->name,
                    'position' => $request->position,
                    'description' => $request->description,
                    'image' => $name_for_db,
                    'created_at' => Carbon::now(),
                ]);
                $toaster = array(
                    'message' => 'You updated successfully new testimonial',
                    'alert-type' => 'info'
                );
                return redirect()->route('testimonial.index')->with($toaster);

            } else {
                Testimonial::find($id)->update([
                    'name' => $request->name,
                    'position' => $request->position,
                    'description' => $request->description,
                    'created_at' => Carbon::now(),
                ]);
                $toaster = array(
                    'message' => 'You updated successfully new testimonial',
                    'alert-type' => 'info'
                );
                return redirect()->route('testimonial.index')->with($toaster);


            }

        }

    }


    public function delete($id)
    {
        $testimonial = Testimonial::find($id);
        $image = $testimonial->image;
        if ($image) {
            unlink($image);
        }
        $testimonial->delete();
        $toaster = array(
            'message' => 'You deleted successfully new testimonial',
            'alert-type' => 'warning'
        );
        return redirect()->route('testimonial.index')->with($toaster);

    }


}


