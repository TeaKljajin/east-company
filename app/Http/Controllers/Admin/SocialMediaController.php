<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SocialMedia;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

class SocialMediaController extends Controller
{


    public function __construct()
    {
        return $this->middleware('auth');
    }


    public function index(){
        $social = SocialMedia::first();
        return view('admin.social_media.index',compact('social'));
    }

    public function facebook(Request $request)
    {
        $social = SocialMedia::where('id',1);
        $validator = Validator::make($request->all(), [
            'facebook' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->route('social.media.index')
                ->withErrors($validator)
                ->withInput();
        } else {

               $social->update([
                    'facebook' => $request->facebook,
                    'updated_at' => Carbon::now(),


                ]);
            }
        $toaster = array(
            'message' => 'You created successfully new facebook link',
            'alert-type' => 'success'
        );
            return redirect()->route('social.media.index')->with($toaster);
        }


        public function facebook_delete(Request $request,$id){
             $facebook = SocialMedia::where('id',1);
             $facebook->update([
                'facebook' => 'null',
                'updated_at' => Carbon::now(),


            ]);
            $toaster = array(
                'message' => 'You deleted successfully facebook link',
                'alert-type' => 'warning'
            );
             return redirect()->route('social.media.index')->with($toaster);
        }

    public function twitter_delete($id){
        $twitter = SocialMedia::where('id',1);
        $twitter->update([
            'twitter' => 'null',
            'updated_at' => Carbon::now(),


        ]);
        $toaster = array(
            'message' => 'You deleted successfully twitter link',
            'alert-type' => 'warning'
        );
        return redirect()->route('social.media.index')->with($toaster);
    }


    public function instagram_delete($id){
        $instagram = SocialMedia::where('id',1);
        $instagram->update([
            'instagram' => 'null',
            'updated_at' => Carbon::now(),


        ]);
        $toaster = array(
            'message' => 'You deleted successfully instagram link',
            'alert-type' => 'warning'
        );
        return redirect()->route('social.media.index')->with($toaster);
    }


    public function pinterest_delete($id){
        $pinterest = SocialMedia::where('id',1);
        $pinterest->update([
            'pinterest' => 'null',
            'updated_at' => Carbon::now(),


        ]);
        $toaster = array(
            'message' => 'You deleted successfully pinterest link',
            'alert-type' => 'warning'
        );
        return redirect()->route('social.media.index')->with($toaster);
    }




    public function twitter(Request $request)
    {
        $social = SocialMedia::where('id',1);
        $validator = Validator::make($request->all(), [
            'twitter' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->route('social.media.index')
                ->withErrors($validator)
                ->withInput();
        } else {

            $social->update([
                'twitter' => $request->twitter,
                'updated_at' => Carbon::now(),


            ]);
        }
        $toaster = array(
            'message' => 'You created successfully new twitter link',
            'alert-type' => 'success'
        );
        return redirect()->route('social.media.index')->with($toaster);
    }

    public function instagram(Request $request)
    {
        $social = SocialMedia::where('id',1);
        $validator = Validator::make($request->all(), [
            'instagram' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->route('social.media.index')
                ->withErrors($validator)
                ->withInput();
        } else {

            $social->update([
                'instagram' => $request->instagram,
                'updated_at' => Carbon::now(),


            ]);
        }
        $toaster = array(
            'message' => 'You created successfully new instagram link',
            'alert-type' => 'success'
        );
        return redirect()->route('social.media.index')->with($toaster);
    }

    public function pinterest(Request $request)
    {
        $social = SocialMedia::where('id',1);
        $validator = Validator::make($request->all(), [
            'pinterest' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->route('social.media.index')
                ->withErrors($validator)
                ->withInput();
        } else {

            $social->update([
                'pinterest' => $request->pinterest,
                'updated_at' => Carbon::now(),


            ]);
        }
        $toaster = array(
            'message' => 'You created successfully new pinterest link',
            'alert-type' => 'success'
        );
        return redirect()->route('social.media.index')->with($toaster);
    }




}
