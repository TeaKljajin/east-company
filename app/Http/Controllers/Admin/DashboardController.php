<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{

    public function __construct(){
        return $this->middleware('auth');
    }


    public function index(){
        $clients = Brand::whereDay('created_at', date('d'))->get();
        dd($clients);
        return view('dashboard',compact('clients'));
    }

    public function logout(){
       Auth::logout();
       return redirect()->route('login')->with('message','You have successfully logout');
    }



}
