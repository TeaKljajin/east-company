<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Models\Service;

class ServiceController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }


    public function index()
    {
        $services = Service::latest()->paginate(5);;

        return view('admin.service.index', compact('services'));
    }


    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'title' => 'required|min:2|max:255',
            'short_des' => 'required|min:2|max:255',
            'image' => 'required|mimes:jpg,jpeg,png',

        ]);

        if ($validator->fails()) {
            return redirect()->route('service.index')
                ->withErrors($validator)
                ->withInput();
        } else {
            //Prepare Image
            $image = $request->file('image');
            $brand_ext = strtolower($image->getClientOriginalExtension());
            $name_gen = hexdec(uniqid());
            $up_location = "images/service/";
            $img_name = $name_gen . '.' . $brand_ext;
            $image->move($up_location, $img_name);
            $name_for_db = $up_location . $img_name;

            Service::insert([
                'title' => $request->title,
                'short_des' => $request->short_des,
                'image' => $name_for_db,
                'created_at' => Carbon::now(),
            ]);
            $toaster = array(
                'message' => 'You created successfully new service',
                'alert-type' => 'success'
            );
            return redirect()->route('service.index')->with($toaster);
        }

    }


    public function edit($id)
    {
        $services = Service::latest()->paginate(5);
        $service = Service::find($id);
        return view('admin.service.edit', compact('services', 'service'));

    }


    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'title' => 'required|min:2|max:255',
            'short_des' => 'max:255',
            'image' => 'mimes:jpg,jpeg,png',
        ]);

        if ($validator->fails()) {
            return redirect()->route('service.edit', $request->id)
                ->withErrors($validator)
                ->withInput();
        } else {

            //Find Old Image
            $service = Service::find($id);
            $old_image = $service->image;

            //New Image
            $image = $request->file('image');

            if ($image) {
                $brand_ext = strtolower($image->getClientOriginalExtension());
                $name_gen = hexdec(uniqid());
                $up_location = "images/brand/";
                $img_name = $name_gen . '.' . $brand_ext;
                $image->move($up_location, $img_name);
                $name_for_db = $up_location . $img_name;

                unlink($old_image);
                Service::find($id)->update([
                    'title' => $request->title,
                    'short_des' => $request->short_des,
                    'image' => $name_for_db,
                    'created_at' => Carbon::now(),
                ]);
                session()->flash('message', 'You updated successfully service');
                return redirect()->route('service.index');

            } else {
                Service::find($id)->update([
                    'title' => $request->title,
                    'short_des' => $request->short_des,
                    'created_at' => Carbon::now(),
                ]);
                $toaster = array(
                    'message' => 'You updated successfully service',
                    'alert-type' => 'info'
                );

                return redirect()->route('service.index')->with($toaster);


            }

        }

    }


    public function delete($id)
    {
        $service = Service::find($id);
        $service->delete();
        $toaster = array(
            'message' => 'You deleted successfully service',
            'alert-type' => 'warning'
        );
        return redirect()->route('service.index')->with($toaster);

    }






}
