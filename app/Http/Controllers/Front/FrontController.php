<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\About;
use App\Models\Brand;
use App\Models\Contact;
use App\Models\Slider;
use App\Models\Portfolio;
use App\Models\Service;
use App\Models\SocialMedia;
use App\Models\Team;
use App\Models\Testimonial;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function index(){
        $brands = Brand::all();
        $sliders = Slider::all();
        $about = About::first();
        $portfolios = Portfolio::all();
        $services = Service::all();
        $teams = Team::all();
        $testimonials = Testimonial::all();
        $social = SocialMedia::first();
        $contact = Contact::first();
        return view('index',compact('brands','sliders','about','portfolios','services','teams','testimonials','social','contact'));
    }

    public function about(){
        $about = About::first();
        $teams = Team::all();
        $testimonials = Testimonial::all();
        $social = SocialMedia::first();
        $contact = Contact::first();

        return view('front.about.index',compact('about','teams','testimonials','social','contact'));
    }


    public function service(){
        $services = Service::all();
        $brands = Brand::all();
        $social = SocialMedia::first();
        $contact = Contact::first();
        return view('front.service.index',compact('services','brands','social','contact'));
    }

    public function portfolio(){
        $portfolios = Portfolio::all();
        $social = SocialMedia::first();
        $contact = Contact::first();
        return view('front.portfolio.index',compact('portfolios','social','contact'));
    }

    public function contact(){
        $contact = Contact::first();
        $social = SocialMedia::first();
        return view('front.contact.index',compact('contact','social'));
    }
}
