-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 27, 2021 at 10:23 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `company_web`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE `abouts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_des` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `long_des` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`id`, `title`, `short_des`, `long_des`, `created_at`, `updated_at`) VALUES
(5, 'EUM IPSAM LABORUM DELENITI VELITENA', 'some.js and.css files in the project. You can find these files in the source code repository under the resources folder. Just copy', 'some.js and.css files in the project. You can find these files in the source code repository under the resources folder. Just copy all of thsome.js and.css files in the project. You can find these files in the source code repository under the resources folder. Just copy all of them (two.js files and a single.css file) and paste them inside the wwwroot folder in the BlazorWasm.Toastr project:em (two.js files and a single.css file) and paste them inside the wwwroot folder in the BlazorWasm.Toastr project:', '2021-04-27 17:32:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `brand_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `brand_name`, `brand_image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'Mobile IO', 'images/brand/1697558905962413.png', '2021-04-20 09:32:09', '2021-04-25 18:25:22', '2021-04-25 18:25:22'),
(4, 'Laptop', 'images/brand/1697558996751542.jpg', '2021-04-20 09:33:35', '2021-04-25 18:25:20', '2021-04-25 18:25:20'),
(12, 'Mobile S', 'images/brand/1698045607566021.png', '2021-04-25 18:28:04', NULL, NULL),
(13, 'Fridged', 'images/brand/1698045624123585.png', '2021-04-25 18:28:19', NULL, NULL),
(14, 'Tvd', 'images/brand/1698045633059422.png', '2021-04-25 18:28:28', NULL, NULL),
(15, 'Laptopd', 'images/brand/1698045642010987.png', '2021-04-25 18:28:36', NULL, NULL),
(16, 'Car', 'images/brand/1698045669008254.png', '2021-04-25 18:29:02', NULL, NULL),
(17, 'Autobus', 'images/brand/1698045682245615.png', '2021-04-25 18:29:15', NULL, NULL),
(18, 'Card', 'images/brand/1698045692579645.png', '2021-04-25 18:29:25', '2021-04-27 17:24:02', '2021-04-27 17:24:02'),
(19, 'Fridgefdfg', 'images/brand/1698222884790305.png', '2021-04-27 17:25:48', '2021-04-27 17:26:54', '2021-04-27 17:26:54'),
(20, 'Fridgetyry', 'images/brand/1698222962013889.png', '2021-04-27 17:27:02', '2021-04-27 17:28:06', '2021-04-27 17:28:06'),
(21, 'Fridgedfgd', 'images/brand/1698223035575153.png', '2021-04-27 17:28:12', '2021-04-27 17:29:24', '2021-04-27 17:29:24'),
(22, 'dfgdfgdfgdf', 'images/brand/1698223119951955.png', '2021-04-27 17:29:33', '2021-04-27 17:29:59', '2021-04-27 17:29:59'),
(23, 'Fridgedfgdfg', 'images/brand/1698223227479582.png', '2021-04-27 17:31:15', '2021-04-27 17:31:19', '2021-04-27 17:31:19'),
(24, 'dfgdfgdfgdfdfgdf', 'images/brand/1698223267876815.png', '2021-04-27 17:31:54', '2021-04-27 17:31:57', '2021-04-27 17:31:57'),
(25, 'Mobiledfgdfg', 'images/brand/1698223411808205.png', '2021-04-27 17:34:11', '2021-04-27 17:34:21', '2021-04-27 17:34:21'),
(26, 'Mobiledfgdfgdgf', 'images/brand/1698223489332971.png', '2021-04-27 17:35:25', '2021-04-27 17:35:28', '2021-04-27 17:35:28'),
(27, 'Fridgedfgdf', 'images/brand/1698225484339622.png', '2021-04-27 18:07:07', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `address`, `city`, `email`, `phone`, `created_at`, `updated_at`) VALUES
(2, 'Tea Kljajin', 'Futoska', 'Novi Sad', 'teakljajin@gmail.com', '064257770954645', '2021-04-27 18:07:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `name`, `email`, `subject`, `description`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Tea Kljajin', 'teakljajin@gmail.com', 'Tablet', 'vbnvbn', 2, '2021-04-24 13:21:44', NULL),
(3, 'fghf', 'fghf@gmail.com', 'fghf', 'fghfg', 2, '2021-04-24 13:22:34', '2021-04-24 13:56:27'),
(4, 'ghjg', 'teakljajin@gmail.com', 'ghjg', 'ghj', 1, '2021-04-24 13:24:53', NULL),
(5, 'John', 'teakljajin@gmail.com', 'fgh', 'fghfgh', 2, '2021-04-24 13:25:13', '2021-04-24 13:59:08'),
(6, 'John', 'teakljajin@gmail.com', 'Tablet', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 2, '2021-04-24 13:30:50', '2021-04-24 13:59:43'),
(8, 'Tea Kljajin', 'teakljajin@gmail.com', 'many variations of passages of Lorem Ipsum availablvariations of passages of Lor', 'ydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.', 2, '2021-04-24 13:34:20', '2021-04-25 13:41:35'),
(9, 'John', 'teakljajin@gmail.com', 'dsfsfs', 'sdfsfsfsdf', 1, '2021-04-25 18:33:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2014_10_12_200000_add_two_factor_columns_to_users_table', 1),
(4, '2019_08_19_000000_create_failed_jobs_table', 1),
(5, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(6, '2021_04_04_184225_create_sessions_table', 1),
(7, '2021_04_20_093300_create_brands_table', 2),
(8, '2021_04_20_125452_create_sliders_table', 3),
(12, '2021_04_21_133300_create_abouts_table', 4),
(13, '2021_04_21_162206_create_portfolios_table', 5),
(14, '2021_04_22_113317_create_services_table', 6),
(15, '2021_04_22_130501_create_teams_table', 7),
(16, '2021_04_22_140024_create_testimonials_table', 8),
(18, '2021_04_24_141719_create_messages_table', 10),
(24, '2021_04_25_061214_create_social_media_table', 11),
(25, '2021_04_24_132758_create_contacts_table', 12),
(26, '2021_04_25_194637_create_newsletters_table', 13);

-- --------------------------------------------------------

--
-- Table structure for table `newsletters`
--

CREATE TABLE `newsletters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `newsletters`
--

INSERT INTO `newsletters` (`id`, `email`, `created_at`, `updated_at`) VALUES
(3, 'teakljajdfgdfgin@gmail.com', '2021-04-25 18:08:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('teakljajin@gmail.com', '$2y$10$81TBqD4zWm3P09EpTq.qruvYv.PRBMUHZEbGtYGAWqGc0ajQ3orvy', '2021-04-26 15:03:17');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `portfolios`
--

CREATE TABLE `portfolios` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `images` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `portfolios`
--

INSERT INTO `portfolios` (`id`, `images`, `created_at`, `updated_at`) VALUES
(1, 'images/portfolio/1697669582784227.png', '2021-04-21 14:51:18', NULL),
(3, 'images/portfolio/1697669830540643.png', '2021-04-21 14:55:15', NULL),
(6, 'images/portfolio/1697669830928337.jpg', '2021-04-21 14:55:15', NULL),
(7, 'images/portfolio/1697669831153791.jpg', '2021-04-21 14:55:15', NULL),
(8, 'images/portfolio/1697669831365784.jpg', '2021-04-21 14:55:16', NULL),
(9, 'images/portfolio/1697669831642659.jpg', '2021-04-21 14:55:16', NULL),
(10, 'images/portfolio/1697669831868887.jpg', '2021-04-21 14:55:16', NULL),
(11, 'images/portfolio/1697669832068656.jpg', '2021-04-21 14:55:16', NULL),
(12, 'images/portfolio/1697669832307346.jpg', '2021-04-21 14:55:17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_des` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `title`, `short_des`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Lorem Ipsum', 'Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi', 'images/service/1697741940140242.png', '2021-04-22 10:01:24', NULL),
(2, 'Sed Perspiciatis', 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore', 'images/service/1697741996623597.jpg', '2021-04-22 10:02:18', NULL),
(3, 'Magni Dolores', 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia', 'images/service/1697742182260107.jpg', '2021-04-22 10:05:15', NULL),
(4, 'Nemo Enim Ri', 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis as', 'images/brand/1697743263303734.png', '2021-04-22 10:05:43', '2021-04-22 10:22:26'),
(5, 'Deleg Cardor', 'Quis consequatur saepe eligendi voluptatem consequatur dolor consequunturrr', 'images/service/1697742237826899.jpg', '2021-04-22 10:06:08', '2021-04-22 10:19:40'),
(7, 'Divera Don', 'Modi nostrum vel laborum. Porro fugit error sit minus sapiente sit aspernatur', 'images/service/1697742390939541.jpg', '2021-04-22 10:08:34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('dMaI3YghbppSv5tNBzlZRC4ErVxoC7RTy4Uyrvh4', 1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 'YTo1OntzOjY6Il90b2tlbiI7czo0MDoiMkI4WG4yTFQ0N05BbkRpOUJIamJVdXlJbDZTWlF6UThJZWtYMkJhbSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mjk6Imh0dHA6Ly8xMjcuMC4wLjE6ODAwMC9wcm9maWxlIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo1MDoibG9naW5fd2ViXzU5YmEzNmFkZGMyYjJmOTQwMTU4MGYwMTRjN2Y1OGVhNGUzMDk4OWQiO2k6MTtzOjE3OiJwYXNzd29yZF9oYXNoX3dlYiI7czo2MDoiJDJ5JDEwJDVJZEwzR21IVlBSaG1BSVBJYmk3bnVPZmw1b256L0dlQThTeGhLa1E1Y0NWZ0doRzZBSVdPIjt9', 1619534091),
('I2pTkvx2KjqDRI7Rl0akU099nhwOnhfWI60TKcDb', 1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 'YTo2OntzOjY6Il90b2tlbiI7czo0MDoiS3ZlT3R1VlEzSEZYcXh6V2loMzRNcFJ0Z0g2U3cxcEhZcGN2aDAzTiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzM6Imh0dHA6Ly8xMjcuMC4wLjE6ODAwMC9icmFuZC9pbmRleCI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fXM6MzoidXJsIjthOjA6e31zOjE3OiJwYXNzd29yZF9oYXNoX3dlYiI7czo2MDoiJDJ5JDEwJDVJZEwzR21IVlBSaG1BSVBJYmk3bnVPZmw1b256L0dlQThTeGhLa1E1Y0NWZ0doRzZBSVdPIjtzOjUwOiJsb2dpbl93ZWJfNTliYTM2YWRkYzJiMmY5NDAxNTgwZjAxNGM3ZjU4ZWE0ZTMwOTg5ZCI7aToxO30=', 1619554907);

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `title`, `description`, `image`, `created_at`, `updated_at`) VALUES
(16, 'Welcome New Customers', 'Today everything is on 50% sale!!!', 'images/slider/1697570502895857.jpg', '2021-04-20 12:05:29', '2021-04-20 12:36:28'),
(17, 'Congratuelations', 'You are getting better', 'images/slider/1697570531210205.png', '2021-04-20 12:36:55', NULL),
(18, 'Three', 'Lorem ipsum sit amet color dopler Lorem ipsum sit amet color dopler Lorem ipsum sit amet color dopler Lorem ipsum sit amet color dopler Lorem ipsum sit amet color dopler Lorem ipsum sit amet color dopler Lorem ipsum sit amet color dopler', 'images/slider/1697571115098096.jpg', '2021-04-20 12:46:12', NULL),
(19, 'New Title', 'fgs sdf  sdfds  sfsdf', 'images/slider/1697664265134782.jpg', '2021-04-21 13:26:47', NULL),
(20, 'iiiiiiiiiiigggggggggggggfghfhhh', 'iiiiiiiiiisdfsd fdfgggggfggggggggg', 'images/slider/1697665032446292.jpg', '2021-04-21 13:26:59', '2021-04-21 13:38:59'),
(21, 'sdfsdf', 'fsdfs', 'images/slider/1697743330535548.jpg', '2021-04-21 13:27:10', '2021-04-22 10:23:30'),
(23, 'EUM IPSAM LABORUM DELENITI VELITENA', 'ghfhfgf', 'images/slider/1697991021840740.jpg', '2021-04-25 04:00:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `social_media`
--

CREATE TABLE `social_media` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `instagram` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `pinterest` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `social_media`
--

INSERT INTO `social_media` (`id`, `facebook`, `twitter`, `instagram`, `pinterest`, `created_at`, `updated_at`) VALUES
(1, 'https://facebook.com', 'https://twitter.com/?lang=en', 'https://www.google.com/', 'http://127.0.0.1:8000/about', NULL, '2021-04-27 17:13:36');

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `name`, `position`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Walter White', 'Chief Executive Officer', 'images/team/1697747265533528.jpg', '2021-04-22 11:26:02', NULL),
(2, 'Sarah Jhonson', 'Product Manager', 'images/team/1697747285292520.jpg', '2021-04-22 11:26:21', NULL),
(3, 'William Anderson', 'CTO', 'images/team/1697747311312154.jpg', '2021-04-22 11:26:46', NULL),
(4, 'Amanda Jepson', 'Accountant', 'images/team/1697747326365101.jpg', '2021-04-22 11:27:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `name`, `position`, `description`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Saul Goodman', 'Ceo & Founder', 'Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.', 'images/testimonial/1697750944548390.jpg', '2021-04-22 12:24:31', NULL),
(2, 'Jena Karlis', 'Store Owner', 'Enim nisi quem export duis labore cillum quae magna enim sint quorum nulla quem veniam duis minim tempor labore quem eram duis noster aute amet eram fore quis sint minim.', 'images/testimonial/1697750976275664.jpg', '2021-04-22 12:25:01', NULL),
(3, 'Sara Wilsson', 'Designer', 'Export tempor illum tamen malis malis eram quae irure esse labore quem cillum quid cillum eram malis quorum velit fore eram velit sunt aliqua noster fugiat irure amet legam anim culpa.', 'images/testimonial/1697751006766169.jpg', '2021-04-22 12:25:30', NULL),
(4, 'Emily Harison', 'Store Owner', 'Eius ipsam praesentium dolor quaerat inventore rerum odio. Quos laudantium adipisci eius. Accusamus qui iste cupiditate sed temporibus est aspernatur. Sequi officiis ea et quia quidem.', 'images/testimonial/1697751036806264.jpg', '2021-04-22 12:25:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_team_id` bigint(20) UNSIGNED DEFAULT NULL,
  `profile_photo_path` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `two_factor_secret`, `two_factor_recovery_codes`, `remember_token`, `current_team_id`, `profile_photo_path`, `created_at`, `updated_at`) VALUES
(1, 'Tea', 'tea@gmail.com', NULL, '$2y$10$5IdL3GmHVPRhmAIPIbi7nuOfl5onz/GeA8SxhKkQ5cCVgGhG6AIWO', NULL, NULL, NULL, NULL, 'images/user/1698204023601292.jpg', '2021-04-04 16:54:37', '2021-04-27 12:26:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletters`
--
ALTER TABLE `newsletters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `portfolios`
--
ALTER TABLE `portfolios`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_user_id_index` (`user_id`),
  ADD KEY `sessions_last_activity_index` (`last_activity`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_media`
--
ALTER TABLE `social_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `newsletters`
--
ALTER TABLE `newsletters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `portfolios`
--
ALTER TABLE `portfolios`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `social_media`
--
ALTER TABLE `social_media`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
